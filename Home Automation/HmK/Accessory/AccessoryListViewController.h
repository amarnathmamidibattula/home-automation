//
//  AccessoryListViewController.h
//  Home Automation
//
//  Created by Santosh on 28/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface AccessoryListViewController : UITableViewController

@property (nonatomic, strong) NSString *roomID;

@end
