//
//  AccessoryListViewController.m
//  Home Automation
//
//  Created by Santosh on 28/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AccessoryListViewController.h"
#import "ServicesViewController.h"
#import "HAHomeManager.h"

@interface AccessoryListViewController () {
    HMRoom *selectedRoom;
}

- (IBAction)onCancelButtonPress:(id)sender;

@end

@implementation AccessoryListViewController

@synthesize roomID;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Room id contains Home name and Room name
    NSArray *components = [roomID componentsSeparatedByString:@"+"];
    NSString *homeName = [components firstObject];
    NSString *roomName = components[1];
    
    HMHomeManager *homeManager = [[HAHomeManager manager] homeManager];
    NSPredicate *homePredicate = [NSPredicate predicateWithFormat:@"name == %@",homeName];
    HMHome *home = (HMHome *)[[[homeManager homes] filteredArrayUsingPredicate:homePredicate] firstObject];
    
    NSLog(@"home :%@ rooms :%@",home, home.rooms);
    
    for (HMRoom* room in home.rooms) {
        NSLog(@"room :%@", room.name);
    }
    
    NSPredicate *roomPredicate = [NSPredicate predicateWithFormat:@"name == %@",roomName];
    selectedRoom = (HMRoom *)[[home.rooms filteredArrayUsingPredicate:roomPredicate] firstObject];
    
    if (selectedRoom) {
        
    } else {
        selectedRoom = [home roomForEntireHome];
    }
    
    [self.tableView reloadData];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([@"AccessoryToServicesSegue" isEqualToString:segue.identifier]) {
        ServicesViewController * viewController = (ServicesViewController *)[segue destinationViewController];
        NSIndexPath * indexPath = (NSIndexPath *)sender;
        HMAccessory * accessory = selectedRoom.accessories[indexPath.row];
        viewController.accessory = accessory;
    }
}

- (IBAction)onCancelButtonPress:(id)sender {
  
    [self dismissViewControllerAnimated:YES
                             completion:^{}];
}


#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [selectedRoom.accessories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *simpleTableIdentifier = @"simpleTableIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    HMAccessory * accessory = [selectedRoom.accessories objectAtIndex:indexPath.row];
    cell.textLabel.text = [accessory name];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"AccessoryToServicesSegue" sender:indexPath];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
