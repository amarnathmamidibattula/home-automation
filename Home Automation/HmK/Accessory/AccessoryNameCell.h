//
//  TableCell.h
//  HmK
//
//  Created by Santosh on 11/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// Custom cell to display browsed accessory and add feature

#import <UIKit/UIKit.h>

@interface AccessoryNameCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIButton *buttonName;
@property (nonatomic, strong) IBOutlet UILabel *textLabel;

@end
