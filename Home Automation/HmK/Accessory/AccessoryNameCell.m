//
//  TableCell.m
//  HmK
//
//  Created by Santosh on 11/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// Custom cell to display browsed accessory and add feature

#import "AccessoryNameCell.h"

@implementation AccessoryNameCell

@synthesize textLabel;


@synthesize buttonName;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
                
    }
    
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
