//
//  AccessoryViewController.h
//  HmK
//
//  Created by Santosh on 11/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ACCESSORIES ADDED TO THE ROOM


#import <UIKit/UIKit.h>
@import HomeKit;

@interface AccessoryViewController : UIViewController
@property (nonatomic, strong) HMRoom *selectedRoom;
@property (nonatomic, strong) HMHome *selectedHome;

@property (nonatomic, strong) HMAccessory *selectedAccessory;

@end
