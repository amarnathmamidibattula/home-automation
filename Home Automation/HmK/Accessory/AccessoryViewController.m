//
//  AccessoryViewController.m
//  HmK
//
//  
//  Created by Santosh on 11/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ACCESSORIES ADDED TO THE ROOM
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];

#import "AccessoryViewController.h"
#import "AccessoryBrowserViewController.h"
#import "ServicesViewController.h"
#import "BeaconDetailViewController.h"
#import "HACoreDataController.h"
#import "BeaconDetailEntity.h"
#import "HAConfiguration.h"

typedef NS_ENUM(NSUInteger, AccessoryTableViewSection) {
    AccessoryTableViewSectionAccessoryContents = 0,
    AccessoryTableViewSectionBeaconContents,
    AccessoryTableViewSectionTotalCount
};

@interface AccessoryViewController () {
    
    BeaconDetailEntity * beaconDetailEntity;
}


@property (nonatomic,weak) IBOutlet UITableView *addedAccessoriesTableView; //Displays List of Accessories
@end

@implementation AccessoryViewController

@synthesize selectedRoom;
@synthesize selectedHome;
@synthesize selectedAccessory;
@synthesize addedAccessoriesTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:selectedRoom.name];
    self.addedAccessoriesTableView.layer.cornerRadius = 10;
    self.addedAccessoriesTableView.layer.masksToBounds = YES;
    [self storeAccessoryName];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomDetail.roomID == %@+%@", selectedHome.name, selectedRoom.name];
    beaconDetailEntity = [[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description]
                                                                                  predicate:predicate
                                                                                    sortKey:nil] firstObject];

    [addedAccessoriesTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([@"AccessoryToServices" isEqualToString:segue.identifier]) {
        ServicesViewController * viewController = (ServicesViewController *)[segue destinationViewController];
        NSIndexPath * indexPath = (NSIndexPath *)sender;
        HMAccessory * accessory = self.selectedRoom.accessories[indexPath.row];
        viewController.accessory = accessory;
    } else if ([@"BrowseAccessorySegue" isEqualToString:segue.identifier]) {
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        AccessoryBrowserViewController *viewController = (AccessoryBrowserViewController *)[navigationController topViewController];
        [viewController setRoom:selectedRoom];
        [viewController setHome:selectedHome];
    } else if ([@"RoomDetailToBrowseBeaconDetail" isEqualToString:segue.identifier]) {
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        BeaconDetailViewController *viewController = (BeaconDetailViewController *)[navigationController topViewController];
        [viewController setRoom:selectedRoom];
        [viewController setHome:selectedHome];
    }
}

- (void)storeAccessoryName {
    
//    NSMutableArray *accessoryList = [[NSMutableArray alloc] init];
//    
//    for (HMAccessory* accessory in selectedRoom.accessories) {
//        [accessoryList addObject:accessory.name];
//    }
//    
//    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.WatchKit.sharing"];    
//    [defaults setValue:accessoryList forKey:@"DataSharing"];
}

#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return AccessoryTableViewSectionTotalCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case AccessoryTableViewSectionAccessoryContents: return [self.selectedRoom.accessories count];
        case AccessoryTableViewSectionBeaconContents: return 1;
        default:  return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    switch (indexPath.section) {
        case AccessoryTableViewSectionAccessoryContents: {
            HMAccessory * accessory = [selectedRoom.accessories objectAtIndex:indexPath.row];
            cell.textLabel.text = [accessory name];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        } break;
        case AccessoryTableViewSectionBeaconContents: {
            if (nil != beaconDetailEntity) {
                // Display Room's added beacon detail
                cell.textLabel.text = [NSString stringWithFormat:@"UUID :%@",[beaconDetailEntity valueForKey:@"uuid"]];
            } else {
                cell.textLabel.text = @"Browse Beacon ...";
            }
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        } break;
            
        default: break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // fixed font style. use custom view (UILabel) if you want something different
    switch (section) {
        case AccessoryTableViewSectionAccessoryContents: return @"Added Accessories";
        case AccessoryTableViewSectionBeaconContents: return @"Beacons";
        default:  return @"";
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {

    switch (section) {
        case AccessoryTableViewSectionAccessoryContents: return @"Tap on accessory to view characteristics";
        case AccessoryTableViewSectionBeaconContents: return @"";
        default:  return @"";
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = BACK_GROUND_HEAD_COLOR;
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 60.0;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case AccessoryTableViewSectionAccessoryContents:
            [self performSegueWithIdentifier:@"AccessoryToServices" sender:indexPath];
            break;
            
        case AccessoryTableViewSectionBeaconContents: {
                if (nil != beaconDetailEntity) {
                    
                } else {
                    [self performSegueWithIdentifier:@"RoomDetailToBrowseBeaconDetail" sender:nil];
                }
            } break;
            
        default:  break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// To delete Saved Accessory Name
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case AccessoryTableViewSectionAccessoryContents: {
            
            HMAccessory *accessory = [self.selectedHome.accessories objectAtIndex:indexPath.row];
            [self.selectedHome removeAccessory:accessory completionHandler:^(NSError *error) {
                NSLog(@"error :%@ ",error);
                [self.addedAccessoriesTableView reloadData];
            }];
        } break;
            
        case AccessoryTableViewSectionBeaconContents: {
            
        } break;
            
        default:  break;
    }
}
     
     
@end
