//
//  AccessoryBrowser.h
//  HmK
//
//  Created by Santosh on 12/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF AVAILABLE ACCESSORIES WHICH CAN BE ADDED TO THE ROOM

#import <UIKit/UIKit.h>
#import "HomeKitViewController.h"
@import HomeKit;
@interface AccessoryBrowserViewController : UIViewController

@property (nonatomic, strong) HMRoom *room;
@property (nonatomic, strong) HMHome *home;
@end
