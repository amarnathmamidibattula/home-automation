//
//  AccessoryBrowser.m
//  HmK
//
//  Created by Santosh on 12/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF AVAILABLE ACCESSORIES WHICH CAN BE ADDED TO THE ROOM


#import "AccessoryBrowserViewController.h"
#import "AccessoryNameCell.h"

@interface AccessoryBrowserViewController () <HMAccessoryBrowserDelegate, HMHomeDelegate>

@property HMAccessoryBrowser *accessoryBrowser;
@property (nonatomic) NSMutableArray *displayedAccessories;
@property (nonatomic) NSMutableArray *addedAccessories;
@property (nonatomic,weak) IBOutlet UITableView *browsedAccessoriesTableView; // Display List of browsed accessories
@property (nonatomic) HMAccessory *selectedAccessory;

- (IBAction)buttonNameClicked:(UIButton*)sender;

@end

@implementation AccessoryBrowserViewController

@synthesize browsedAccessoriesTableView;
@synthesize room;
@synthesize home;

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    self.accessoryBrowser = [HMAccessoryBrowser new];
    self.displayedAccessories = [NSMutableArray array];
    self.accessoryBrowser.delegate = self;
    self.addedAccessories = [NSMutableArray array];
    
    [self startBrowsing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  Starts browsing for accessories.
 */
- (void)startBrowsing {
    [self.accessoryBrowser startSearchingForNewAccessories];
    //NSLog(@"Browsing Started ");
}

/**
 *  Stops browsing for accessories.
 */
- (void)stopBrowsing {
    [self.accessoryBrowser stopSearchingForNewAccessories];
   // NSLog(@"Browsing Stopped ");
    
}


#pragma mark - HMAccessoryBrowserDelegate methods

- (void)accessoryBrowser:(HMAccessoryBrowser *)browser didFindNewAccessory:(HMAccessory *)accessory
{
    // NSArray *discoveredAccessories = self.accessoryBrowser.discoveredAccessories;
    
    //NSLog(@"accessory :%@",discoveredAccessories);
    [self.displayedAccessories addObject:accessory];
    [self.browsedAccessoriesTableView reloadData];
    
}

- (void)accessoryBrowser:(HMAccessoryBrowser *)browser didRemoveNewAccessory:(HMAccessory *)accessory {
    [self reloadTable];
}
#pragma mark - Private Methods

// Adds accessory to room
- (IBAction)buttonNameClicked:(UIButton*)sender {
    __weak typeof(self) weakSelf = self;
    NSLog(@"You have pressed the button");
    HMAccessory *accessory = self.accessoryBrowser.discoveredAccessories[sender.tag];

    [home addAccessory:accessory completionHandler:^(NSError *error) {
       
        if (nil == error) {
            [weakSelf.home assignAccessory:accessory toRoom:weakSelf.room completionHandler:^(NSError *error) {
                if (nil == error) {
                    [weakSelf.self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                } else {
                    NSLog(@"assign accessory error :%@",error);
                }
                
            }];
        } else {
                    NSLog(@"add accessory error :%@",error);
        }
    }];
}

- (void)cancel: (id)sender{
      [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Delegate and Datasource Methods

#pragma mark UITableView Data Source Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSUInteger rows = self.accessoryBrowser.discoveredAccessories.count;
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellIdentifier";
    AccessoryNameCell *cell = (AccessoryNameCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    // Get the current accessory associated with this index.
    HMAccessory *accessory = self.accessoryBrowser.discoveredAccessories[indexPath.row];
    
    cell.textLabel.text = [accessory name];
    cell.buttonName.tag = indexPath.row;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //HMAccessory* accessory = self.displayedAccessories[indexPath.row];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    // fixed font style. use custom view (UILabel) if you want something different
{
    return [NSString stringWithFormat:@"Showing available accessories: "];
}

#pragma mark - ModifyAccessoryDelegate

/**
 *  Adds the accessory to our list of accessories added in this session,
 *
 *  @param viewController The accessory view controller.
 *  @param accessory      The accessory that was saved.
 */


#pragma mark - Accessory Storage

/**
 *  Resets the saved accessory list to both the discovered accessories
 *  and the ones added in this session.
 */
- (void)resetDisplayedAccessories {
    //self.displayedAccessories = [self allAccessories];
   // NSLog(@"Browsing Step13 ");
}

/**
 *  @discussion Stores an alphabetized cache of the accessory browser's accessories.
 *  @return The stored accessoryBrowser's discoveredAccessories, alphabetized.
 */
- (NSArray *)allAccessories {
    //NSLog(@"Browsing Step14 ");
    NSMutableArray *allAccessories = [NSMutableArray array];
    NSArray *discoveredAccessories = self.accessoryBrowser.discoveredAccessories;
    if (discoveredAccessories) {
        [allAccessories addObjectsFromArray:discoveredAccessories];
    }
    [allAccessories addObjectsFromArray:self.addedAccessories];
    
    
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                     ascending:YES
                                                                      selector:@selector(caseInsensitiveCompare:)];
    return [allAccessories sortedArrayUsingDescriptors:@[nameDescriptor]];
}

/**
 *  Invalidates our cache of alphabetized accessories and reloads the tableView's data.
 */
- (void)reloadTable {
    //NSLog(@"Browsing Step15 ");
    [self resetDisplayedAccessories];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.browsedAccessoriesTableView reloadData];
    });
}


- (void)configureAccessory:(HMAccessory *)accessory {
    NSUInteger index = [self.displayedAccessories indexOfObject:accessory];
    if (index == NSNotFound) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.selectedAccessory = accessory;
        [self performSegueWithIdentifier:@"Add Accessory" sender:self];
    });
}

- (HMAccessory *)unconfiguredHomeKitAccessoryWithName:(NSString *)name {
    NSLog(@"Browsing Step17 ");
    if (!name)
        return nil;
    for (HMAccessory *accessory in self.displayedAccessories) {
        if ([accessory isKindOfClass:HMAccessory.class] && [accessory.name isEqualToString:name]) {
            return accessory;
        }
    }
    return nil;
}
@end
