//
//  ActionSetTableViewCell.h
//  HmK
//
//  Created by Santosh on 06/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionSetTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *actionSetTextField;
@property (nonatomic, strong) IBOutlet UILabel *actionSetLabel;

@end
