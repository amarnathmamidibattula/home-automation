//
//  ActionSetTableViewCell.m
//  HmK
//
//  Created by Santosh on 06/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "ActionSetTableViewCell.h"

@implementation ActionSetTableViewCell

@synthesize actionSetTextField;
@synthesize actionSetLabel;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        actionSetTextField = [[UITextField alloc] initWithFrame:CGRectMake(14, 7, 300, 30)];
        [actionSetTextField setBorderStyle:UITextBorderStyleNone];
        [actionSetTextField setFont:[UIFont systemFontOfSize:17.0f]];
        [actionSetTextField setDelegate:self];
        [self.contentView addSubview:actionSetTextField];
        
        actionSetLabel = [[UILabel alloc] init];
        [actionSetLabel setFrame:CGRectMake(20, 6, 300, 20)];
        [actionSetLabel setFont:[UIFont systemFontOfSize:17.0]];
        [actionSetLabel setTextColor:[UIColor grayColor]];
        [self.contentView addSubview:actionSetLabel];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - DELEGATE METHODS
#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
