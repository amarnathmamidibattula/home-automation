//
//  ActionSetViewController.h
//  HmK
//
//  Created by Santosh on 01/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface ActionSetViewController : UIViewController
/**
 *  The home to which this action set will be added.
 */
@property (nonatomic, strong) HMHome *selectedHome;
/**
 *  The action set which will be updated by this creator.
 */
@property (nonatomic, strong) HMActionSet *selectedActionSet;

@end
