//
//  ActionSetViewController.m
//  HmK
//
//  Created by Santosh on 01/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#define SERVICE_SEGUE               @"ActionSetToServiceSegue"
#define ACTION_SET_TEXTFIELD_TAG    10000
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];

#import "ActionSetViewController.h"
#import "ServicesViewController.h"
#import "ActionSetTableViewCell.h"
#import "CharacteristicTableViewCell.h"
#import "HMCharacteristic+Readability.h"
#import "NSError+HomeKit.h"

typedef NS_ENUM(NSUInteger, ActionSetTableViewSection) {
    ActionSetTableViewSectionName = 0,
    ActionSetTableViewSectionAction,
    ActionSetTableViewSectionAccessory
};

@interface ActionSetViewController ()<UITableViewDataSource, UITableViewDelegate, CharacteristicCellDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem * saveButton;
@property (nonatomic, weak) IBOutlet UITableView *actionSetTableView;
@property (nonatomic) NSMapTable *targetValueMap;

/**
 *  Method call on save button pressed
 *
 *  @param sender UIButton Instance
 */
- (IBAction)onSaveButtonPress:(id)sender;
- (IBAction)onCancelButtonPress:(id)sender;
/**
 *  Iterates over a map table of HMCharacteristic -> id objects and creates
 *  an array of HMCharacteristicWriteActions based on those targets.
 *
 *  @param table An NSMapTable mapping HMCharacteristics to id's.
 *
 *  @return An array of HMCharacteristicWriteActions.
 */
- (NSArray *)actionsFromMapTable:(NSMapTable *)table;
/**
 *  Checks to see if an action already exists to modify the same characteristic as the action passed in.
 *  If such an action exists, the method tells the existing action to update its target value.
 *  Otherwise, the new action is simply added to the action set.
 *
 *  @param action     The action to add or update.
 *  @param actionSet  The action set to which to add the action.
 *  @param completion A block to call when the addition has finished.
 */
- (void)addAction:(HMCharacteristicWriteAction *)action toActionSet:(HMActionSet *)actionSet completion:(void (^)(NSError *))completion;
@end

@implementation ActionSetViewController

@synthesize saveButton;
@synthesize actionSetTableView;
@synthesize targetValueMap;
@synthesize selectedHome;
@synthesize selectedActionSet;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Create the structure we're going to use to hold the target values.
    self.targetValueMap = [NSMapTable strongToStrongObjectsMapTable];
    self.actionSetTableView.layer.cornerRadius = 10;
    self.actionSetTableView.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    // Reload Table View
    //[actionSetTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    [actionSetTableView reloadData];
    [self enableSaveButtonIfApplicable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([SERVICE_SEGUE isEqualToString:segue.identifier]) {
        // Display Service View Controller
        ServicesViewController * viewController = (ServicesViewController *)[segue destinationViewController];
        NSIndexPath * indexPath = (NSIndexPath *)sender;
        HMAccessory * accessory = selectedHome.accessories[indexPath.row];
        [viewController setCellDelegate:self];
        viewController.accessory = accessory;
    }
}

#pragma mark - Private Methods
#pragma mark UIButton Action Method
- (IBAction)onCancelButtonPress:(id)sender {
    // Dismiss controller
    [self dismissViewControllerAnimated:YES
                             completion:^{}];
}

- (IBAction)onSaveButtonPress:(id)sender {
    
    UITextField* actionSetNameTextField = (UITextField *)[actionSetTableView viewWithTag:ACTION_SET_TEXTFIELD_TAG];
    if (self.selectedActionSet) {
        [self saveActionSet:self.selectedActionSet];
        [self updateNameIfNecessary:actionSetNameTextField.text];
    } else {
        [self createActionSetWithName:actionSetNameTextField.text];
    }
    
    [self dismissViewControllerAnimated:YES
                             completion:^{}];
}

- (void)nameFieldDidChange:(UITextField *)sender {
    [self enableSaveButtonIfApplicable];
}

#pragma mark - MISC Methods

- (BOOL)containsActions {
    return [[self allCharacteristics] count] > 0;
}

- (NSArray *)allCharacteristics {
    NSMutableSet *characteristics = [NSMutableSet set];
    for (HMCharacteristicWriteAction *action in self.selectedActionSet.actions) {
        [characteristics addObject:action.characteristic];
    }
    for (HMCharacteristic *characteristic in self.targetValueMap.keyEnumerator.allObjects) {
        [characteristics addObject:characteristic];
    }
    return characteristics.allObjects;
}

- (id)targetValueForCharacteristic:(HMCharacteristic *)characteristic {
    id value = [self.targetValueMap objectForKey:characteristic];
    if (!value) {
        for (HMCharacteristicWriteAction *action in self.selectedActionSet.actions) {
            if ([action.characteristic isEqual:characteristic]) {
                value = action.targetValue;
            }
        }
    }
    return value;
}

- (void)enableSaveButtonIfApplicable {
    UITextField* actionSetNameTextField = (UITextField *)[actionSetTableView viewWithTag:ACTION_SET_TEXTFIELD_TAG];
    // Enable the save button if the text field has something in it.
    self.saveButton.enabled = actionSetNameTextField.text.length > 0 && [self containsActions];
}

#pragma mark Save Action Set

- (void)createActionSetWithName:(NSString *)name {
    __weak typeof(self) weakSelf = self;
    [self.selectedHome addActionSetWithName:name completionHandler:^(HMActionSet *actionSet, NSError *error) {
        if (error) {
            NSLog(@"Error creating action set: %@", error.hmc_localizedTranslation);
//            weakSelf.saveError = error;
        } else {
            [weakSelf saveActionSet:actionSet];
        }
    }];
}

- (void)saveActionSet:(HMActionSet *)actionSet {
    NSArray *actions = [self actionsFromMapTable:self.targetValueMap];
    for (HMCharacteristicWriteAction *action in actions) {
//        __weak typeof(self) weakSelf = self;
        [self addAction:action toActionSet:actionSet completion:^(NSError *error) {
            if (error) {
                NSLog(@"Error adding action: %@", error.hmc_localizedTranslation);
//                weakSelf.saveError = error;
            }
        }];
    }
}

- (NSArray *)actionsFromMapTable:(NSMapTable *)table {
    NSMutableArray *actions = [NSMutableArray array];
    for (HMCharacteristic *key in table) {
        HMCharacteristicWriteAction *action = [[HMCharacteristicWriteAction alloc] initWithCharacteristic:key targetValue:[table objectForKey:key]];
        [actions addObject:action];
    }
    return actions;
}

- (void)addAction:(HMCharacteristicWriteAction *)action toActionSet:(HMActionSet *)actionSet completion:(void (^)(NSError *))completion {
    HMCharacteristicWriteAction *existingAction = [self existingActionInActionSetMatchingAction:action];
    if (existingAction) {
        [existingAction updateTargetValue:action.targetValue completionHandler:completion];
    } else {
        [actionSet addAction:action completionHandler:completion];
    }
}

- (HMCharacteristicWriteAction *)existingActionInActionSetMatchingAction:(HMCharacteristicWriteAction *)action {
    for (HMCharacteristicWriteAction *existingAction in self.selectedActionSet.actions) {
        if ([action.characteristic isEqual:existingAction.characteristic]) {
            return existingAction;
        }
    }
    return nil;
}

- (void)updateNameIfNecessary:(NSString *)name {
    if ([self.selectedActionSet.name isEqualToString:name]) {
        return;
    }
    [self.selectedActionSet updateName:name completionHandler:^(NSError *error) {
        if (error) {
            NSLog(@"Error updating name: %@", error.hmc_localizedTranslation);
//            self.saveError = error;
        }
    }];
}

#pragma mark Remove Characteristic from action set

- (void)removeTargetValueForCharacteristicIndexPath:(NSIndexPath *)indexPath completionHandler:(void (^)())completion {
    // Remove selected indexPath action from action set or map table
    HMCharacteristic *characteristic = [[self allCharacteristics] objectAtIndex:indexPath.row];
    BOOL isDeleted = NO;
    
    if ([self.targetValueMap objectForKey:characteristic]) {
        // Remove the characteristic from the target value map.
        [self.targetValueMap removeObjectForKey:characteristic];
    }
    for (HMCharacteristicWriteAction *action in self.selectedActionSet.actions) {
        if ([action.characteristic isEqual:characteristic]) {
            // Also remove the action, and only relinquish the dispatch group
            // once the action set has finished.
            isDeleted = YES;
            [self.selectedActionSet removeAction:action completionHandler:^(NSError *error) {
                if (error) {
                    NSLog(@"error %@", error.hmc_localizedTranslation);
                }
                completion ();
            }];
        }
    }

    if (!isDeleted) {
        completion ();
    }
}

#pragma mark - Delegate
#pragma mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
 // Default is 1 if not implemented
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case ActionSetTableViewSectionName:
            return 1;
        case ActionSetTableViewSectionAction:
            return MAX([[self allCharacteristics] count], 1);
        case ActionSetTableViewSectionAccessory:
            return selectedHome.accessories.count;
        default:
            return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // fixed font style. use custom view (UILabel) if you want something different
    switch (section) {
        case ActionSetTableViewSectionName:
            return @"Action Name";
        case ActionSetTableViewSectionAction:
            return @"Action";
        case ActionSetTableViewSectionAccessory:
            return @"List of Acessories";
        default:
            return @"";
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == ActionSetTableViewSectionAction;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self removeTargetValueForCharacteristicIndexPath:indexPath completionHandler:^() {
        [tableView beginUpdates];
        if ([self containsActions]) {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else {
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [tableView endUpdates];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                        reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    // Configure Cell
    switch (indexPath.section) {
        case ActionSetTableViewSectionName:
            return [self tableView:tableView actionNameCellForRowAtIndexPath:indexPath];
        case ActionSetTableViewSectionAction:
            return [self tableView:tableView actionCellForRowAtIndexPath:indexPath];
        case ActionSetTableViewSectionAccessory:
            return [self tableView:tableView accessoryCellForRowAtIndexPath:indexPath];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = BACK_GROUND_HEAD_COLOR;
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

#pragma mark Custom Cell Methods
- (ActionSetTableViewCell *)tableView:(UITableView *)tableView actionNameCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"actionCellIdentifier";
    ActionSetTableViewCell *cell = (ActionSetTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[ActionSetTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:cellIdentifier];
    }
    // Display Text field
    [cell.actionSetTextField setText:self.selectedActionSet.name];
    [cell.actionSetTextField setPlaceholder:@"Enter Action Name"];
    [cell.actionSetTextField setTag:ACTION_SET_TEXTFIELD_TAG];
    [cell.actionSetTextField addTarget:self action:@selector(nameFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView actionCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"actionCellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    // Configure action cell
    cell.textLabel.textColor = [UIColor blackColor];

    if ([self containsActions]) {
        // Display all the added ActionSet's Characterstics value
        HMCharacteristic *characteristic = [[self allCharacteristics] objectAtIndex:indexPath.row];
        id value = [self targetValueForCharacteristic:characteristic];
        
        NSString *targetDescription = [NSString stringWithFormat:@"%@ → %@", characteristic.hmc_localizedCharacteristicType, [characteristic hmc_localizedDescriptionForValue:value]];
        cell.textLabel.text = targetDescription;
        NSString *contextDescription = NSLocalizedString(@"%@ in %@", @"Service in Accessory");
        cell.detailTextLabel.text = [NSString stringWithFormat:contextDescription, characteristic.service.name, characteristic.service.accessory.name];
        
    } else {
        // Display Message, If no action is added yet
        cell.textLabel.text = NSLocalizedString(@"No Actions Yet",@"");
        cell.detailTextLabel.text = @"";
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView accessoryCellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"accessoryCellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Display list of accessory in the home
    HMAccessory *accessory = selectedHome.accessories[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = accessory.name;
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case ActionSetTableViewSectionName :
            break;
        case ActionSetTableViewSectionAction :
            break;
        case ActionSetTableViewSectionAccessory :
            // Display selected accessory service list
            [self performSegueWithIdentifier:SERVICE_SEGUE sender:indexPath];
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark CharacteristicCellDelegate Methods
/**
 *  Receives a callback from a CharacteristicCell with a value change.
 *  Adds this value change into the targetValueMap, overwriting other value changes.
 */
- (void)characteristicCell:(CharacteristicTableViewCell *)cell
            didUpdateValue:(id)newValue
         forCharacteristic:(HMCharacteristic *)characteristic {
    // Add updated characterstic value content
    [self.targetValueMap setObject:newValue forKey:characteristic];
}

- (void)characteristicCell:(CharacteristicTableViewCell *)cell
        readInitialValueForCharacteristic:(HMCharacteristic *)characteristic
                completion:(void (^)(id value, NSError *error))completion {
    
    // Check to see if we have an action in this Action Set that matches the characteristic.
    // If we do, call the completion block with the target value.
    NSArray *targetValueMapActions = self.targetValueMap.keyEnumerator.allObjects;
    for (HMCharacteristic *_characteristic in targetValueMapActions) {
        if ([_characteristic isEqual:characteristic]) {
            completion([self.targetValueMap objectForKey:characteristic], nil);
            return;
        }
    }
    NSArray *actions = [self.selectedActionSet.actions allObjects];
    for (HMCharacteristicWriteAction *action in actions) {
        if ([action.characteristic isEqual:characteristic]) {
            completion(action.targetValue, nil);
            return;
        }
    }
    
    // If we haven't exited the function yet, fall back to just reading the value.
    [characteristic readValueWithCompletionHandler:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(characteristic.value, error);
        });
    }];

}

@end
