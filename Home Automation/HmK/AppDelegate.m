//
//  AppDelegate.m
//  HmK
//
//  Created by Santosh on 10/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AppDelegate.h"
#import "CloudkitController.h"
#import "AccessoryListViewController.h"
#import "HAConfiguration.h"
#import "EMBeaconManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    

    [[CloudkitController dataHandler] checkForiCloudAccountStatusWithCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"iCloud access error %@", error);
            // Display SignIn iCloud Alert
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Sign in to iCloud"
                                                                 message:@"Sign in to your iCloud account to write records. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn iCloud Drive on. If you don't have an iCloud account, tap Create a new Apple ID."
                                                                delegate:nil
                                                       cancelButtonTitle:@"Okay"
                                                       otherButtonTitles: nil];
            [alertView show];
        } else {
            
            [[CloudkitController dataHandler] fetchRecordsWithType:@"BeaconDetail" predicate:[NSPredicate predicateWithFormat:@"TRUEPREDICATE"] recordID:nil completionBlock:^(NSArray *results, NSError *error) {
                
                if (results && [results count]) {
                    [[CloudkitController dataHandler] storeiCloudBeaconDetail:results completion:^{
                        // Start Beacon Detection Process
                        [[EMBeaconManager manager] initLocationManager];
                    }];
                } else {
                }
            }];
        }

    }];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
        
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
//    [self saveContext];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    // Called when the application receive local notification.
    
    NSLog(@"notification :%@",notification);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[notification.userInfo valueForKey:@"title"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
    [alertView show];
    // Only Post Notification for Event Beacon Recognition
    if (DisplayAccessoriesList == [[notification.userInfo valueForKeyPath:@"notificationType"] integerValue]) {
        // Room id contains Home name and Room name
        [self displayAccessoriesListScreenWithRoomID:[notification.userInfo valueForKeyPath:@"roomID"]];
    }
}

- (void)displayAccessoriesListScreenWithRoomID:(NSString *)roomID {
    
    // Display Room's Accessories
    // Fetch AccessoryListNavigationController and Present it in RootViewController
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *navigationController = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"AccessoryListNavigationController"];
    AccessoryListViewController* viewController = (AccessoryListViewController *)[navigationController topViewController];
    [viewController setRoomID:roomID];
    
    //  Temporary iOS8 fix for 'presentation lag' on launch
    // [self.window addSubview:navigationController.topViewController.view];
    [self.window.rootViewController presentViewController:navigationController
                                                     animated:YES
                                                   completion:^{
                                                       
                                                       // [self.window removeFromSuperview];
                                                   }];
}

@end
