//
//  AddBeaconTableViewCell.h
//  Home Automation
//
//  Created by Santosh on 26/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AddBeaconTableViewCell;
@protocol AddBeaconTableViewCellDelegate <NSObject>

- (void)addBeaconTableViewCell:(AddBeaconTableViewCell *)cell didAddButtonPress:(UIButton *)button;

@end

@interface AddBeaconTableViewCell : UITableViewCell

@property (nonatomic, weak) id<AddBeaconTableViewCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UITextField *beaconTextField;
@property (nonatomic, weak) IBOutlet UIButton *addBeaconButton;

@end
