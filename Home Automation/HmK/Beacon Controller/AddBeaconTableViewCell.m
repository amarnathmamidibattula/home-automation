//
//  AddBeaconTableViewCell.m
//  Home Automation
//
//  Created by Santosh on 26/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AddBeaconTableViewCell.h"

@interface AddBeaconTableViewCell ()

- (IBAction)onAddBeaconButtonPress:(UIButton *)sender;

@end

@implementation AddBeaconTableViewCell

@synthesize delegate;
@synthesize titleLabel;
@synthesize backgroundImageView;
@synthesize beaconTextField;
@synthesize addBeaconButton;

- (IBAction)onAddBeaconButtonPress:(UIButton *)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(addBeaconTableViewCell:didAddButtonPress:)] &&
        [self.delegate conformsToProtocol:@protocol(AddBeaconTableViewCellDelegate)]) {
        [self.delegate addBeaconTableViewCell:self didAddButtonPress:sender];
    }
}

@end
