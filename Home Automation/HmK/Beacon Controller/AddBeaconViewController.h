//
//  AddBeaconViewController.h
//  Home Automation
//
//  Created by Santosh on 26/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBeaconViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *addBeaconTable ;

@end
