//
//  AddBeaconViewController.m
//  Home Automation
//
//  Created by Santosh on 26/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AddBeaconViewController.h"
#import "AddBeaconTableViewCell.h"
#import "UITextField+AKNumericFormatter.h"
#import "AKNumericFormatter.h"
#import "CloudkitController.h"
#import "CloudkitParams.h"

typedef NS_ENUM(NSInteger, CellTypeBeacon) {
    CellTypeBeaconName = 0,
    CellTypeBeaconUUID ,
    CellTypeBeaconMajor ,
    CellTypeBeaconMinor ,
    CellTypeBeaconAddButton ,
    CellTypeBeaconCount
};

@interface AddBeaconViewController () <UITextFieldDelegate, AddBeaconTableViewCellDelegate> {
    
    NSMutableArray *beaconDetail;
    UITextField *activeTextField;
}

- (IBAction)onCancleButtonPress:(id)sender;

@end

@implementation AddBeaconViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = NO;
    // Initialize Registeration Content
    beaconDetail = [[NSMutableArray alloc] init];
    self.addBeaconTable.layer.cornerRadius = 10;
    self.addBeaconTable.layer.masksToBounds = YES;
    [self refreshTableContent];
}

#pragma mark - Private Methods

- (void)refreshTableContent {
    
    [beaconDetail removeAllObjects];
    [beaconDetail setArray:@[@{@"Title":@"Beacon Name", @"Placeholder":@"Enter Beacon Name", @"Content":@""},
                             @{@"Title":@"Beacon UUID", @"Placeholder":@"Enter Beacon UUID", @"Content":@""},
                             @{@"Title":@"Major", @"Placeholder":@"Enter Major value", @"Content":@""},
                             @{@"Title":@"Minor", @"Placeholder":@"Enter Minor value", @"Content":@""}]];
    [self.addBeaconTable reloadData];
}

#pragma mark API Call

- (void)storeBeaconDetailWithCompletionBlock:(void (^)(NSError *error))completion {
    
    // API Parameters
    NSDictionary *params = @{HACloudBeaconNameAttributeName:[[beaconDetail objectAtIndex:CellTypeBeaconName] valueForKey:@"Content"],
                             HACloudUUIDAttributeName:[[beaconDetail objectAtIndex:CellTypeBeaconUUID] valueForKey:@"Content"],
                             HACloudMajorAttributeName:[[beaconDetail objectAtIndex:CellTypeBeaconMajor] valueForKey:@"Content"],
                             HACloudMinorAttributeName:[[beaconDetail objectAtIndex:CellTypeBeaconMinor] valueForKey:@"Content"],
                             HACloudIndexAttributeName:[NSDate date]
                             };
    
    [[CloudkitController dataHandler] storeBeaconDetailsOniCloud:params
                                                 completionBlock:^(CKRecord *beaconRecord, NSError *error) {
                         if (!error) {
                             // Insert successfully saved beacon record code
                             [[CloudkitController dataHandler] storeiCloudBeaconDetail:@[beaconRecord]
                                                                            completion:^{
                                                                                
                                                                                completion (nil);
                             }];
                             
                         } else {
                             // Insert error handling
                             NSLog(@"beaconRecord error :%@",error);
                             completion (error);
                         }
                     }];
}

#pragma mark IBActions
- (IBAction)onCancleButtonPress:(id)sender {
    // Dismiss Controller
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Delegate Methods
#pragma mark UITableView Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return CellTypeBeaconCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddBeaconTableViewCell *cell = nil;
    static NSString *cellIdentifier = @"cellIdentifier";
    cell = (AddBeaconTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    [cell.titleLabel setFont:[UIFont fontWithName:ROBOTO_REGULAR size:12]];
//    [cell.signUpTextField setFont:[UIFont fontWithName:ROBOTO_LIGHT size:14]];

    // Configure Cell...
    switch (indexPath.row) {
        case CellTypeBeaconName:
        {
            // For First name
            [cell.beaconTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
        } break;
        
        case CellTypeBeaconUUID:
        {
            //B9407F30-F5F8-466E-AFF9-25556B57FE6D
            [cell.beaconTextField setKeyboardType:UIKeyboardTypePhonePad];
            [cell.beaconTextField setNumericFormatter:[AKNumericFormatter formatterWithMask:@"********-****-****-****-************" placeholderCharacter:'*']];
        } break;
            
        case CellTypeBeaconMajor:
        case CellTypeBeaconMinor:
        {
            [cell.beaconTextField setKeyboardType:UIKeyboardTypePhonePad];
        } break;
            
        case CellTypeBeaconAddButton:
        {
            // Add Beacon Button
            [cell.addBeaconButton setHidden:NO];
            [cell.beaconTextField setHidden:YES];
            [cell.titleLabel setHidden:YES];
            [cell.backgroundImageView setHidden:YES];
        } break;
    }
    
    [cell setDelegate:self];
    // Configure Cell
    if (indexPath.row < CellTypeBeaconAddButton) {
        [cell.addBeaconButton setHidden:YES];
        [cell.beaconTextField setHidden:NO];
        [cell.titleLabel setHidden:NO];
        [cell.backgroundImageView setHidden:NO];
        
        cell.titleLabel.text = [[beaconDetail objectAtIndex:indexPath.row] valueForKey:@"Title"];
        cell.beaconTextField.placeholder = [[beaconDetail objectAtIndex:indexPath.row] valueForKey:@"Placeholder"];
        cell.beaconTextField.text = [[beaconDetail objectAtIndex:indexPath.row] valueForKey:@"Content"];
        cell.beaconTextField.tag = indexPath.row;
        cell.beaconTextField.delegate = self;
    }

    return cell;
}
#pragma mark UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 62.0f;
}

#pragma mark - AddBeaconTableViewCell Delegate

- (void)addBeaconTableViewCell:(AddBeaconTableViewCell *)cell didAddButtonPress:(UIButton *)button {
    
    // Is any text field active?
    if (activeTextField && [activeTextField isFirstResponder]) {
        [activeTextField resignFirstResponder];
    }

    [self storeBeaconDetailWithCompletionBlock:^(NSError *error) {
        if (nil == error) {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Beacon detail store successfully" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            [self refreshTableContent];
        } else {
            // Show Alert
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[error description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        }
    }];
}

#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // return NO to disallow editing.
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // became first responder
    activeTextField = textField;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
    if ([beaconDetail count] >= textField.tag) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[beaconDetail objectAtIndex:textField.tag]];
        [dic setValue:textField.text forKey:@"Content"];
        [beaconDetail replaceObjectAtIndex:textField.tag withObject:dic];
    }
    
    activeTextField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // called when 'return' key pressed. return NO to ignore.
    [textField resignFirstResponder];
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    // return NO to not change text
//    
//    NSString *substring = [NSString stringWithString:textField.text];
//    NSString* text = [substring stringByReplacingCharactersInRange:range withString:string];
//    
//    //User name character length must be lower than 64 character.
//    if ([text length]>kMaxCharLength) {
//        return NO;
//    } else {
//        return YES;
//    }
//}

@end
