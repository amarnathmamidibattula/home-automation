//
//  BeaconDetailViewController.h
//  Home Automation
//
//  Created by Santosh on 08/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@import HomeKit;
@interface BeaconDetailViewController : UIViewController

@property (nonatomic, strong) HMRoom *room;
@property (nonatomic, strong) HMHome *home;
@property (nonatomic, strong) IBOutlet UITableView *beaconDetailTable;

@end
