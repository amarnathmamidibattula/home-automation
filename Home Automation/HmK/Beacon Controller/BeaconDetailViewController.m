//
//  BeaconDetailViewController.m
//  Home Automation
//
//  Created by Santosh on 08/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "BeaconDetailViewController.h"
#import "HACoreDataController.h"
#import "BeaconDetailEntity.h"
#import "RoomDetailEntity.h"
#import "CloudkitController.h"
#import "CloudkitParams.h"

@interface BeaconDetailViewController () {
    
    NSMutableArray * beaconDetail;
}

- (IBAction)onCancleButtonPress:(id)sender;

@end

@implementation BeaconDetailViewController

@synthesize room;
@synthesize home;
@synthesize beaconDetailTable;

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextObjectsDidChangeNotification:) name:NSManagedObjectContextObjectsDidChangeNotification object:nil];
    // Initialize beacon array contents
    beaconDetail = [[NSMutableArray alloc] init];
    // Display Table Contents
    [self refreshTableContents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    // Remove Observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextObjectsDidChangeNotification object:nil];
}

#pragma mark - Private Methods

- (void)refreshTableContents {
    [beaconDetail removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomDetail == nil"];
    [beaconDetail setArray:[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description]
                                                                                   predicate:predicate
                                                                                     sortKey:nil]];
    [self.beaconDetailTable reloadData];
}

#pragma mark IBActions
- (IBAction)onCancleButtonPress:(id)sender {
    // Dismiss Controller
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark Notification Methods
- (void)contextObjectsDidChangeNotification:(NSNotification *)notify {
    // Reload  the table view
    [self refreshTableContents];
}

#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Default is 1 if not implemented
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [beaconDetail count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"beaconNameCellIdentifier" forIndexPath:indexPath];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    // Fetch beacon entity instance
    BeaconDetailEntity *beaconDetailEntity = [beaconDetail objectAtIndex:indexPath.row];
    // Display beacon name
    cell.textLabel.text = [beaconDetailEntity valueForKey:@"beaconName"];
    cell.textLabel.textColor=[UIColor blackColor];
    
    return cell;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:[UIColor colorWithRed:240.0/255 green:239.0/255 blue:245.0/255 alpha:1.0]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // Create Relationship between Room Detail Record and Beacon Detail Record
    BeaconDetailEntity *beaconDetailEntity = [beaconDetail objectAtIndex:indexPath.row];

    CKRecordID *roomRecordID = [[CKRecordID alloc] initWithRecordName:[NSString stringWithFormat:@"%@+%@", home.name, room.name]];
    CKReference *roomReference = [[CKReference alloc] initWithRecordID:roomRecordID action:CKReferenceActionDeleteSelf];
    
    CKRecordID *beaconRecordID = [[CKRecordID alloc] initWithRecordName:[NSString stringWithFormat:@"%@", beaconDetailEntity.uuid]];
    CKRecord *beaconRecord = [[CKRecord alloc] initWithRecordType:HACloudKitBeaconRecordType recordID:beaconRecordID];
    [beaconRecord setObject:roomReference forKey:@"RoomDetail"];
    
    [[CloudkitController dataHandler] storeRecord:beaconRecord
                                completionHandler:^(CKRecord *record, NSError *error) {
                                    
                                    if (nil == error) {
                                        // Fetch room detail entity
                                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomID == %@+%@", home.name, room.name];
                                        NSArray *roomDetail = [[HACoreDataController sharedInstance] fetchManageObjectsWithName:[RoomDetailEntity description]
                                                                                                                      predicate:predicate
                                                                                                                        sortKey:nil];
                                        RoomDetailEntity * roomDetailEntity = [roomDetail firstObject];
                                        [beaconDetailEntity setValue:roomDetailEntity forKey:@"roomDetail"];
                                        // Save Context
                                        [[HACoreDataController sharedInstance] saveContext];
                                    } else {
                                        
                                    }
                                    
                                    NSLog(@" ======= room results :%@", record);
                                    NSLog(@" ======= error :%@", error);
                                    
    }];
    
    /*
    // Fetch Room and Beacon detail
    [[CloudkitController dataHandler] fetchRecordsWithType:HACloudRoomDetailAttributeName
                                                 predicate:[NSPredicate predicateWithFormat:@"%@ == %@+%@", HACloudRoomIDAttributeName, home.name, room.name]
                                                  recordID:nil
                                           completionBlock:^(NSArray *results, NSError *error) {
                                               NSLog(@" ======= room results :%@", results);
                                               
                                               CKRecord *roomRecord = (CKRecord *)[results firstObject];
                                               CKReference *roomReference = [[CKReference alloc] initWithRecordID:roomRecord.recordID action:CKReferenceActionDeleteSelf];
                                              
                                               [[CloudkitController dataHandler] fetchRecordsWithType:HACloudKitBeaconRecordType
                                                                                            predicate:[NSPredicate predicateWithFormat:@"%@ == %@", HACloudUUIDAttributeName, beaconDetailEntity.uuid]
                                                                                             recordID:nil
                                                                                      completionBlock:^(NSArray *results, NSError *error) {
                                                                                          NSLog(@"======== beacon results :%@", results);
                                                                                          
                                                                                          CKRecord *beaconRecord = (CKRecord *)[results firstObject];
                                                                                          [beaconRecord setObject:roomReference forKey:@"RoomDetail"];

                                                                                          NSString *uuid = [beaconRecord objectForKey:HACloudUUIDAttributeName];
                                                                                          NSLog(@"======= uuid :%@",uuid);
                                                                                      }];

                                           }];
     */
}
@end
