//
//  EMBeaconManager.h
//  EventManagement
//
//  Created by Santosh on 04/02/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"

@class EMBeaconManager;

@protocol EMBeaconManagerDelegate <NSObject>

- (void)beacon:(EMBeaconManager *)manager didRangeBeacons:(NSArray *)beacons;

@end

@interface EMBeaconManager : NSObject

@property (nonatomic, weak) id delegate;
/*
 Create the singleton object of 'EMBeaconManager' class.
 */
+ (instancetype)manager;
/*
 * Initialize location manager
 */
- (void)initLocationManager;
/**
 *  Get selected Boardcasting power in ESTBeaconPower enum
 *
 *  @param level selected power lever
 *
 *  @return Return ESTBeaconPower enum.
 */
- (NSInteger)getBeaconPower:(NSInteger)level;
/**
 *  Method used for display Beacon Color
 *
 *  @param beaconColor Beacon Color
 *
 *  @return Return UI Color Instance
 */
- (UIColor *)estimoteBeaconColor:(ESTBeaconColor )beaconColor;
@end
