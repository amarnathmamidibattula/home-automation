//
//  EMBeaconManager.m
//  EventManagement
//
//  Created by Santosh on 04/02/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//


#define SAMPLING_COUNT              6

#import "EMBeaconManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "HAConfiguration.h"
#import "EMLocationManager.h"
#import "EMLocation.h"
#import "EMBeacon.h"
#import "ESTConfig.h"

#import "HACoreDataController.h"
#import "BeaconDetailEntity.h"
#import "RoomDetailEntity.h"
//#import "EMCoreDataController.h"
//#import "EMBeaconDataController.h"
//#import "EMUserDataController.h"
//#import "SessionEntity.h"
//#import "UserEntity.h"

@interface EMBeaconManager ()<CBPeripheralManagerDelegate, CBCentralManagerDelegate, ESTBeaconManagerDelegate, ESTBeaconDelegate> {
    
    EMLocationManager *locationManager;
    ESTBeaconManager *beaconManager;
    CBPeripheralManager *peripheralManager;
    NSMutableArray *samplingArray;
    
    BOOL isSessionEnter;
}

@property (nonatomic, strong) EMLocationManager *locationManager;
@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, readwrite) BOOL isBeaconDetected;
/**
 *  Start Location Monitoring
 */
- (void)startLocationMonitoring;
/**
 *  Start Beacon Monitoring
 */
- (void)startBeaconMonitoringUsingEstimote;
/**
 *  Perfrom action according to beacon state
 *
 *  @param beacons list of all detect beacons in the region
 */
- (void)handleStateOfDetectedBeacons:(NSArray *)beacons;
/**
 *  Method used to check found Beacon Proximity State
 *
 *  @param foundBeacon Detected Beacon
 */
- (void)checkBeaconProximityState:(ESTBeacon *)foundBeacon;
/**
 *  Fire Local Notification.
 *
 *  @param title alert body text message
 */
- (void)fireLocalNotificationWithTitle:(NSString *)title
                                roomID:(NSString *)roomID
                                  type:(LocalNotificationType)type;
/**
 *  Method used for hit SessionEntryExit API
 *
 *  @param detectedBeaconUUID beacon proximity UUID
 *  @param isEntry            whether user enter into sesion or exit from the sesion
 *  @param completionBlock    Returns Response
 */
- (void)storeSessionEntryExitTimeWithProximityUUID:(NSString *)detectedBeaconUUID
                                           isEntry:(BOOL)isEntry
                                   completionBlock:(void (^)(id responseObject, NSString *error))completionBlock;

@end

@implementation EMBeaconManager

@synthesize delegate;
@synthesize locationManager;
@synthesize beaconManager;
@synthesize isBeaconDetected;

static EMBeaconManager *manager;
#pragma mark - Allocate Singleton Instance
+ (instancetype)manager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Allocate singleton instance
        manager = [[EMBeaconManager alloc] init];
    });
    
    return manager;
}

+ (id)alloc {
    @synchronized(self) {
        NSAssert(manager == nil, @"Attempted to allocate a second instance of 'EMBeaconManager' class");
        manager = [super alloc];
    }
    
    return manager;
}

#pragma mark - Public Methods

- (void)initLocationManager {
    // Initialize Sampling Array
    samplingArray = [[NSMutableArray alloc] init];
    // Initalize location manager
    locationManager = [[EMLocationManager alloc] init];
    locationManager.delegate = self;
    // Start Location Monitoring
    [self startLocationMonitoring];
}

- (NSInteger)getBeaconPower:(NSInteger)level {
    NSInteger beaconPower = 0;
    
    switch (level) {
        case 0: beaconPower = ESTBeaconPowerLevel1;
            break;
        case 1: beaconPower = ESTBeaconPowerLevel2;
            break;
        case 2: beaconPower = ESTBeaconPowerLevel3;
            break;
        case 3: beaconPower = ESTBeaconPowerLevel4;
            break;
        case 4: beaconPower = ESTBeaconPowerLevel5;
            break;
        case 5: beaconPower = ESTBeaconPowerLevel6;
            break;
        case 6: beaconPower = ESTBeaconPowerLevel7;
            break;
        case 7: beaconPower = ESTBeaconPowerLevel8;
            break;
        default:
            break;
    }
    
    return beaconPower;
}

- (UIColor *)estimoteBeaconColor:(ESTBeaconColor )beaconColor {

    UIColor * color = nil;

    switch (beaconColor) {
        case ESTBeaconColorMint:
            break;
        case ESTBeaconColorIce:
            break;
        case ESTBeaconColorBlueberry:
            break;
        case ESTBeaconColorWhite:
            color = [UIColor whiteColor];
            break;
        case ESTBeaconColorTransparent:
            break;
        case ESTBeaconColorUnknown:
            color = [UIColor whiteColor];
            break;
        default:
            color = [UIColor whiteColor];
            break;
    }
    
    return color;
}

#pragma mark - Private Methods

- (void)startLocationMonitoring {
    
    /*
     Noida - VI location detail:-
     latitude :28.620515 longitude :77.376721
     */
    // FIXME::For now, Dummy location is used
    EMLocation *location = [[EMLocation alloc] initWithLocationName:@"Test Location"
                                                           latitude:EVENT_LOCATION_LATITUDE
                                                          longitude:EVENT_LOCATION_LONGITUDE
                                                             radius:EVENT_LOCATION_RADIUS];
    // Start monitoring for circular location
    [locationManager startMonitoringForCircularRegion:location];
}

- (void)startBeaconMonitoringUsingEstimote {
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    self.beaconManager.returnAllRangedBeaconsAtOnce = YES;
    
    switch ([ESTBeaconManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined: {
            
            if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
                /*
                 * No need to explicitly request permission in iOS < 8, will happen automatically when starting ranging.
                 */
               NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomDetail != nil"];
               NSArray* beaconDetail = [[NSArray alloc] initWithArray:[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description]
                                                                                                                              predicate:predicate
                                                                                                                                sortKey:nil]];
                // Start Ranging beacons
                [self startRangingBeacons:beaconDetail];
            } else {
                /*
                 * Request permission to use Location Services. (new in iOS 8)
                 * We ask for "always" authorization so that the Notification Demo can benefit as well.
                 * Also requires NSLocationAlwaysUsageDescription in Info.plist file.
                 *
                 * For more details about the new Location Services authorization model refer to:
                 * https://community.estimote.com/hc/en-us/articles/203393036-Estimote-SDK-and-iOS-8-Location-Services
                 */
                [self.beaconManager requestAlwaysAuthorization];
            }
        } break;
            
        case kCLAuthorizationStatusAuthorizedAlways: {
            // Start Beacon Ranging
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomDetail != nil"];
            NSArray* beaconDetail = [[NSArray alloc] initWithArray:[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description]
                                                                                                                           predicate:predicate
                                                                                                                             sortKey:nil]];
            // Start Ranging beacons
            [self startRangingBeacons:beaconDetail];
        } break;
            
        case kCLAuthorizationStatusDenied: {
            // Display Error MEssage for location service is not available
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Location Access Denied"
                                                            message: @"You have denied access to location services. Change this in app settings."
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        } break;
            
        case kCLAuthorizationStatusRestricted: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Location Not Available"
                                                            message: @"You have no access to location services."
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil];
            [alert show];

        } break;
            
        default: break;
    }
}

- (void)startRangingBeacons:(NSArray *)beacons {
    [beacons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BeaconDetailEntity *entity = (BeaconDetailEntity *)obj;
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",[entity valueForKeyPath:@"uuid"]]];
        ESTBeaconRegion *region = [[ESTBeaconRegion alloc] initWithProximityUUID:uuid
                                                                           major:[[entity valueForKeyPath:@"major"] integerValue]
                                                                           minor:[[entity valueForKeyPath:@"minor"] integerValue]
                                                                      identifier:[entity valueForKeyPath:@"identifier"]];
        [self.beaconManager startRangingBeaconsInRegion:region];
    }];
}

- (void)handleStateOfDetectedBeacons:(NSArray *)beacons {
    
    // Enumerate All the Detected Beacons
    [beacons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

        ESTBeacon *foundBeacon = (ESTBeacon *)obj;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid == %@ && major == %@ && minor == %@",foundBeacon.proximityUUID, foundBeacon.major, foundBeacon.minor];
        NSArray *localBeaconDetail = [[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description] predicate:predicate sortKey:nil];
        
        // Check Whether detected beacon has same UUID or not?// @"B9407F30-F5F8-466E-AFF9-25556B57FE6E"
        if ([localBeaconDetail count] && NO == isBeaconDetected) {
        
            isBeaconDetected = YES;
            // Room detected.
            BeaconDetailEntity *beaconDetailEntity = (BeaconDetailEntity *)[localBeaconDetail firstObject];
            RoomDetailEntity *roomDetailEntity = beaconDetailEntity.roomDetail;
            // Fire Local Notification
            // Display Room's Accessories
            [self fireLocalNotificationWithTitle:[NSString stringWithFormat:@"Welcome to the %@ room",roomDetailEntity.roomName]
                                          roomID:roomDetailEntity.roomID
                                            type:DisplayAccessoriesList];
        } else {
            
        }
        
        /*if ([HALL1_PROXIMITY_UUID isEqualToString:[[foundBeacon proximityUUID] UUIDString]]) {
        
            NSInteger index;
            BOOL flagNear = false;
            BOOL flagFar = false;
            
            [samplingArray addObject:foundBeacon];

            if (SAMPLING_COUNT <= [samplingArray count]) {
                // Checking for NEAR proximity Sampling
                index = [samplingArray count] - SAMPLING_COUNT;
                
                for (int counter = index; counter < [samplingArray count]; counter++) {
                    
                    ESTBeacon *foundBeacon = (ESTBeacon *)[samplingArray objectAtIndex:counter];
                    if ((CLProximityNear == foundBeacon.proximity) || (CLProximityImmediate == foundBeacon.proximity)) {
                        flagNear = TRUE;
                        flagFar = FALSE;
                    } else {
                        flagNear = FALSE;
                        flagFar = FALSE;
                        break;
                    }
                }
                
                if (flagNear) {
                    // Display 'Welcome' message
                    [self checkBeaconProximityState:foundBeacon];
                    [samplingArray removeAllObjects];
                } else {
                    // Check Far Condition Sampling
                    for (int counter = index; counter < [samplingArray count]; counter++) {
                        if (CLProximityFar == foundBeacon.proximity) {
                            flagNear = FALSE;
                            flagFar = TRUE;
                        } else {
                            flagNear = FALSE;
                            flagFar = FALSE;
                            break;
                        }
                    }
                    
                    if (flagFar) {
                        // Display 'Exit' message
                        [self checkBeaconProximityState:foundBeacon];
                        [samplingArray removeAllObjects];
                    }
                }
            }
        } */
        
    }];
}

- (void)checkBeaconProximityState:(ESTBeacon *)foundBeacon {
    /*
    // Check Only those beacons if there Proximity is IMMEDIATE OR NEAR.
    switch (foundBeacon.proximity) {
        case CLProximityImmediate:
        case CLProximityNear: {
            if (NO == isSessionEnter) {
                isSessionEnter = YES;
                
//                [self fireLocalNotificationWithTitle:@"Welcome to the session" type:WelcomeNoteType];
//                // Store Session Entry Time on Google Cloud
//                [self storeSessionEntryExitTimeWithProximityUUID:[foundBeacon.proximityUUID UUIDString]
//                                                         isEntry:YES
//                                                 completionBlock:^(id responseObject, NSString *error) {
//                                                     DebugLog(@"storeSessionEntryExitTimeWithProximityUUID error :%@",error);
//                                                 }];
                
                // [[EMBeaconDataController dataHandler] beaconDetectedinNearProximity:foundBeacon];
            }
            
        } break;
            
        case CLProximityFar: {
//            DebugLog(@"Far ======== Accuracy: %f ======== RSSI: %d",[foundBeacon.distance floatValue],foundBeacon.rssi);
//
//            if (YES == isSessionEnter) {
//                isSessionEnter = NO;
//            
//                if (FAR_DISTANCE_MIN_RANGE < [foundBeacon.distance floatValue] &&
//                    FAR_RSSI_MIN_RANGE < foundBeacon.rssi) {
//                    // Fire Notification
//                    [self fireLocalNotificationWithTitle:@"Thank you for attending the session"
//                                                    type:WelcomeNoteType];
//                    // Store Session Exit Time on Google Cloud
//                    [self storeSessionEntryExitTimeWithProximityUUID:[foundBeacon.proximityUUID UUIDString]
//                                                             isEntry:NO
//                                                     completionBlock:^(id responseObject, NSString *error) {
//                                                         DebugLog(@"storeSessionEntryExitTimeWithProximityUUID error :%@",error);
//                                                     }];
//                }
//            }
        } break;
            
        case CLProximityUnknown: break;
            
        default: break;
    }
    */
}
    
- (void)fireLocalNotificationWithTitle:(NSString *)title
                                roomID:(NSString *)roomID
                                  type:(LocalNotificationType)type {
    
    // Fire local notification
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = title;
    notification.userInfo = @{@"title": title, @"notificationType": @(type), @"roomID": roomID};
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (void)storeSessionEntryExitTimeWithProximityUUID:(NSString *)detectedBeaconUUID
                                           isEntry:(BOOL)isEntry
                                   completionBlock:(void (^)(id responseObject, NSString *error))completionBlock {
    
    completionBlock(nil, @"Beacon not found");
   /*
//    detectedBeaconUUID = @"B9407F30-F5F8-466E-AFF9-25556B57FE6E";
    // Get the Original Beacon (BeaconEntity) instance from Core Data
    // which has same UUID
//   NSArray * originalBeacons = [[EMCoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconEntity description]
//                                                                                       predicate:[NSPredicate predicateWithFormat:@"uuid == %@",detectedBeaconUUID]
//                                                                                         sortKey:nil];
//    // Check Whethere any beacon found in Coredata which has same UUID
//    if ([originalBeacons count]) {
//        // if Beacon instance found in Core Data,
//        // Then check which hall number it is located
//        // using beaconId and current timing
//        BeaconEntity *beaconEntity = [originalBeacons firstObject];
//        // Get list of session instance
//        NSArray *sessions = [[EMCoreDataController sharedInstance] fetchManageObjectsWithName:[SessionEntity description]
//                                                                                    predicate:[NSPredicate predicateWithFormat:@"originalBeaconId == %@",beaconEntity.beaconId]
//                                                                                      sortKey:nil];
//        if ([sessions count]) {
//            // List of Session Found
//            NSDate *currentDate = [NSDate date];
////            NSDate *currentDate = [EMUtilityManager dateFromString:@"2015-01-09 11:55:00" dateFormat:@"YYYY-MM-dd HH:mm:ss"];
//            NSArray *currentlyRunningSession = [sessions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"startDate <= %@ && endDate >= %@",currentDate, currentDate]];
//            
//            if ([currentlyRunningSession count]) {
//                SessionEntity *sessionEntity = (SessionEntity *)[currentlyRunningSession firstObject];
//                __block NSDictionary *params = nil;
//                [[EMUserDataController dataHandler] userAlreadyLogin:^(BOOL isLogin, UserEntity *userEntity) {
//                    if (isEntry) {
//                        params = @{@"sessionId":[sessionEntity valueForKey:@"sessionId"], @"userId": [userEntity valueForKey:@"userID"], @"entryTime":currentDate, @"exitTime":@""};
//                    } else {
//                        params = @{@"sessionId":[sessionEntity valueForKey:@"sessionId"], @"userId": [userEntity valueForKey:@"userID"], @"entryTime":@"", @"exitTime":currentDate};
//                    }
//                }];
//                
//                // Hit Session Entry API
//                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//                [manager POST:[NSString stringWithFormat:@"%@%@",APIBaseURLString, AdminSessionEntryAPI]
//                   parameters:params
//                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                          
//                          DebugLog(@"AdminSessionEntryAPI responseObject:%@",responseObject);
//                          completionBlock(responseObject, nil);
//                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                          
//                          DebugLog(@"AdminSessionEntryAPI error:%@",error);
//                          completionBlock(nil, error.description);
//                      }];
//                
//            } else {
//                // Do nothing
//                completionBlock(nil, @"No Session is occuring now");
//            }
//        } else {
//            // Do nothing
//            completionBlock(nil, @"Session not found");
//        }
//    } else {
//        // Do nothing
//        completionBlock(nil, @"Beacon not found");
//    }
    */
}

#pragma mark - Delegate Method
#pragma mark EMLocationManagerDelegate Protocol Method

- (void)location:(EMLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLCircularRegion class]]) {
         // Check for bluetooth enable or disabled.
         // Start the peripheral manager
        peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                                    queue:nil
                                                                  options:nil];
    }
//    else if ([region isKindOfClass:[CLBeaconRegion class]]) {
//        // Start Beacon Ranging for the region.
//        [locationManager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
//    }
}

- (void)location:(EMLocationManager *)manager didExitRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        // User out of the geofence region.
        // Stop ranging beacons in the region
    } else if ([region isKindOfClass:[CLBeaconRegion class]]) {
        
    }
}

- (void)location:(EMLocationManager *)manager
 didRangeBeacons:(NSArray*)beacons
        inRegion:(CLBeaconRegion*)region {
    
    [self handleStateOfDetectedBeacons:beacons];
}

- (void)location:(EMLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region
       withError:(NSError *)error {
    // Start Location Monitoring
    [self startLocationMonitoring];
}

#pragma mark CBPeripheralManagerDelegate Protocol Method
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager*)peripheral {
    
//    DebugLog(@"peripheral state :%d",peripheral.state);
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn: {
            // Bluetooth is on
            // Fetch All the Beacons value from local data base.
            // Start Monitoring using Estimote SDK
            [self startBeaconMonitoringUsingEstimote];
            [peripheralManager setDelegate:nil];
        } break;
            
        case CBPeripheralManagerStatePoweredOff: {
            // Bluetooth is off
            [self fireLocalNotificationWithTitle:@"Please Turn On Bluetooth"
                                          roomID:nil
                                            type:BluetoothOffType];
        } break;
            
        case CBPeripheralManagerStateUnsupported: {
            DebugLog(@"CBPeripheralManagerStateUnsupported");
        } break;
            
        case CBPeripheralManagerStateUnauthorized: {
            DebugLog(@"CBPeripheralManagerStateUnauthorized");
        } break;
            
        default: break;
    }
}

#pragma mark CBCentralManagerDelegate Methods

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    DebugLog(@"centralManagerDidUpdateState");
}

#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(ESTBeaconManager *)manager rangingBeaconsDidFailForRegion:(ESTBeaconRegion *)region withError:(NSError *)error
{
    DebugLog(@"Ranging error :%@", error.localizedDescription);
}

- (void)beaconManager:(ESTBeaconManager *)manager monitoringDidFailForRegion:(ESTBeaconRegion *)region withError:(NSError *)error
{
    //DebugLog(@"Monitoring error :%@", error.localizedDescription);
    // Start Location Monitoring
    [self startLocationMonitoring];
}

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    // We can do beacon detection action here.
    [self handleStateOfDetectedBeacons:beacons];

    if (delegate && [delegate conformsToProtocol:@protocol(EMBeaconManagerDelegate)] &&
        [delegate respondsToSelector:@selector(beacon:didRangeBeacons:)]) {

        [delegate beacon:self didRangeBeacons:beacons];
    }
}

#pragma mark - ESTBeaconDelegate Methods

/**
 * Tells the delegate that an attempt to connect to a beacon succeeded and the connection has been established.
 *
 * @param beacon The beacon object reporting the event.
 */
- (void)beaconConnectionDidSucceeded:(ESTBeacon *)beacon {
 
    DebugLog(@"beaconConnectionDidSucceeded :%@", beacon);
}

/**
 * Tells the delegate that an attempt to connect to a beacon has failed.
 *
 * @param beacon The beacon object reporting the event.
 * @param error An error object containing the error code that indicates why connection failed.
 */
- (void)beaconConnectionDidFail:(ESTBeacon *)beacon withError:(NSError *)error {
        DebugLog(@"beaconConnectionDidFail :%@ error :%@", beacon, error);
}

/**
 * Tells the delegate that a previously connected beacon has disconnected.
 *
 * @param beacon The beacon object reporting the event.
 * @param error An error object containing the error code that indicates why the beacon disconnected.
 */
- (void)beacon:(ESTBeacon *)beacon didDisconnectWithError:(NSError *)error {
    DebugLog(@"didDisconnectWithError :%@ error :%@", beacon, error);

}

@end