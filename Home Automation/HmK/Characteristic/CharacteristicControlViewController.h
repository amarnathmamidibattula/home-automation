//
//  CharacteristicControlViewController.h
//  HmK
//
//  Created by Santosh on 19/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAY AND CONTROL THE LIST OF CHARACTERISTICS

#import <UIKit/UIKit.h>
#import "CharacteristicTableViewCell.h"

@import HomeKit;

@interface CharacteristicControlViewController : UIViewController

@property (nonatomic, strong) HMService *service;
@property (nonatomic) id<CharacteristicCellDelegate> cellDelegate;

@end
