//
//  CharacteristicControlViewController.m
//  HmK
//
//  Created by Santosh on 19/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAY AND CONTROL THE LIST OF CHARACTERISTICS
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];

#import "CharacteristicControlViewController.h"
#import "HMCharacteristic+Properties.h"
#import "HMCharacteristic+Readability.h"
#import "NSError+HomeKit.h"

@interface CharacteristicControlViewController ()<UITableViewDataSource, UITableViewDelegate, HMAccessoryDelegate, CharacteristicCellDelegate>

@property (nonatomic,weak) IBOutlet UITableView *characteristicControlTableView;
@property (nonatomic) NSMapTable *targetValueMap;
@property (nonatomic, strong) NSTimer *updateValueTimer;

@end

@implementation CharacteristicControlViewController

@synthesize characteristicControlTableView;
@synthesize service;
@synthesize cellDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [HMCharacteristic initialize];
    NSLog(@"service.accessory :%@",service.accessory);
    [service.accessory setDelegate:self];
    [self.navigationItem setTitle:service.name];
    [self startListeningForCellUpdates];
    self.characteristicControlTableView.layer.cornerRadius = 10;
    self.characteristicControlTableView.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    for (HMCharacteristic *thisCharacteristic in service.characteristics) {
        if ([thisCharacteristic.properties containsObject:HMCharacteristicPropertySupportsEventNotification]) {
            [thisCharacteristic enableNotification:TRUE completionHandler:^(NSError *error) {
                if (error) {
                    NSLog(@"Error while enabling notification");
                }
            }];
        }
    }
    
    [characteristicControlTableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    for (HMCharacteristic *thisCharacteristic in service.characteristics) {
        if ([thisCharacteristic.properties containsObject:HMCharacteristicPropertySupportsEventNotification]) {
            [thisCharacteristic enableNotification:FALSE completionHandler:^(NSError *error) {
                if (error) {
                    NSLog(@"Error while disabling notification");
                }
            }];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    cellDelegate = nil;
    [self stopListeningForCellUpdates];
}


#pragma mark - Private Method
/**
 *  Registers for CharacteristicCell update notifications and starts the coalesced
 *  characteristic writing timer.
 */
- (void)startListeningForCellUpdates {
    self.updateValueTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                             target:self
                                                            selector:@selector(updateCharacteristics)
                                                            userInfo:nil
                                                          repeats:YES];
    self.targetValueMap = [NSMapTable strongToStrongObjectsMapTable];
}

/**
 *  Invalidates the timer and resets the target value map.
 */
- (void)stopListeningForCellUpdates {
    [self.updateValueTimer invalidate];
     self.targetValueMap = nil;
}


#pragma mark - HMAccessoryDelegate Methods
- (void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic {

    NSUInteger index = [self.service.characteristics indexOfObject:characteristic];
    if (index != NSNotFound) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [characteristicControlTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}


#pragma mark - UITableView Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return service.characteristics.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CharacteristicTableViewCell *cell = nil;
    HMCharacteristic *characteristic = self.service.characteristics[indexPath.row];

    // Dequeue once, choose reuse identifiers in if/else blocks
    NSString *cellReuseIdentifier = @"CharacteristicCell";
    cell = (CharacteristicTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
    if (!cell) {
        cell = [[CharacteristicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseIdentifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // Configure Cell
    [cell setCharacteristic:characteristic];
    [cell setDelegate:self];
    
    [cell.switchControl setHidden:YES];
    [cell.segmentControl setHidden:YES];
    [cell.sliderControl setHidden:YES];
    [cell.valueLabel setHidden:YES];
    
    if (characteristic.hmc_isReadOnly || characteristic.hmc_isWriteOnly) {
        // For Read Only Property
        [cell.valueLabel setHidden:NO];
        CGRect valueLabelFrame = cell.valueLabel.frame;
        valueLabelFrame.size.width = 280;
        [cell.valueLabel setFrame:valueLabelFrame];
    } else if (characteristic.hmc_isBoolean) {
        // For Switch Button
        [cell.switchControl setHidden:NO];
        // Display Switch Value
        [cell.valueLabel setHidden:NO];
        CGRect valueLabelFrame = cell.valueLabel.frame;
        valueLabelFrame.size.width = 80;
        [cell.valueLabel setFrame:valueLabelFrame];
    } else if (characteristic.hmc_hasPredeterminedValueDescriptions) {
        // For Segment Control
        [cell.segmentControl setHidden:NO];
        [cell.segmentControl removeAllSegments];
        [characteristic.hmc_allPossibleValues enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [cell.segmentControl insertSegmentWithTitle:[characteristic hmc_localizedDescriptionForValue:obj]
                                                atIndex:cell.segmentControl.numberOfSegments
                                               animated:NO];
        }];
    } else if (characteristic.hmc_isNumeric) {
        // For Display Slider
        [cell.sliderControl setHidden:NO];
        cell.sliderControl.minimumValue = characteristic.metadata.minimumValue.doubleValue;
        cell.sliderControl.maximumValue = characteristic.metadata.maximumValue.doubleValue;
        // Display Slider Value
        [cell.valueLabel setHidden:NO];
        CGRect valueLabelFrame = cell.valueLabel.frame;
        valueLabelFrame.size.width = 80;
        [cell.valueLabel setFrame:valueLabelFrame];
    } else {
        
    }

    [cell.typeLabel setText:characteristic.hmc_localizedCharacteristicType];
    [cell updatedValue];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    // fixed font style. use custom view (UILabel) if you want something different
{
    return @"Characteristics";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 70.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = BACK_GROUND_HEAD_COLOR;
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 60.0;
}
#pragma mark CharacteristicTableViewCell Delegate Methods

- (void)characteristicCell:(CharacteristicTableViewCell *)cell
            didUpdateValue:(id)newValue
         forCharacteristic:(HMCharacteristic *)characteristic {
    
    if (cellDelegate && [cellDelegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] &&
        [cellDelegate respondsToSelector:@selector(characteristicCell:didUpdateValue:forCharacteristic:)]) {
        [cellDelegate characteristicCell:cell didUpdateValue:newValue forCharacteristic:characteristic];
    } else {
        
        [self.targetValueMap setObject:newValue forKey:characteristic];
        [self updateCharacteristics];
    }
}

- (void)characteristicCell:(CharacteristicTableViewCell *)cell
        readInitialValueForCharacteristic:(HMCharacteristic *)characteristic
                completion:(void (^)(id value, NSError *error))completion {

    if (cellDelegate && [cellDelegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] &&
        [cellDelegate respondsToSelector:@selector(characteristicCell:readInitialValueForCharacteristic:completion:)]) {

        [cellDelegate characteristicCell:cell readInitialValueForCharacteristic:characteristic completion:^(id value, NSError *error) {
            completion(value, error);
        }];
        
    } else {
        
        [characteristic readValueWithCompletionHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(characteristic.value, error);
            });
        }];
    }
}

- (void)updateCharacteristics {
    
    for (HMCharacteristic *characteristic in self.targetValueMap) {
        id newValue = [self.targetValueMap objectForKey:characteristic];
        [characteristic writeValue:newValue completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Could not change value: %@", error.hmc_localizedTranslation);
                return;
            }
             //[self.characteristicControlTableView reloadData];
        }];
    }
    [self.targetValueMap removeAllObjects];
}

@end
