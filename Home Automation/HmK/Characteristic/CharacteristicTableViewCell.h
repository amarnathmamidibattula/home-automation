//
//  CharacteristicTableViewCell.h
//  HmK
//
//  Created by Santosh on 19/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// Custom cell to display charecteristics control and display features

#import <UIKit/UIKit.h>
@import HomeKit;

@class CharacteristicTableViewCell;


@protocol CharacteristicCellDelegate <NSObject>

- (void)characteristicCell:(CharacteristicTableViewCell *)cell didUpdateValue:(id)newValue forCharacteristic:(HMCharacteristic *)characteristic;

- (void)characteristicCell:(CharacteristicTableViewCell *)cell readInitialValueForCharacteristic:(HMCharacteristic *)characteristic completion:(void (^)(id value, NSError *error))completion;

@end


@interface CharacteristicTableViewCell : UITableViewCell

@property (weak, nonatomic) id<CharacteristicCellDelegate> delegate;
@property (weak, nonatomic) HMCharacteristic *characteristic;
@property (nonatomic, strong) UISwitch *switchControl;
@property (nonatomic, strong) UISegmentedControl *segmentControl;
@property (nonatomic, strong) UISlider *sliderControl;
@property (nonatomic, strong) UILabel *typeLabel;               // To display characteristic type
@property (nonatomic, strong) UILabel *valueLabel;              // To display value of characteristic

- (void)updatedValue;
@end
