//
//  CharacteristicTableViewCell.m
//  HmK
//
//  Created by Santosh on 19/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// Custom cell to display charecteristics control and display features
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];
#import "CharacteristicTableViewCell.h"
#import "HMCharacteristic+Readability.h"
#import "HMCharacteristic+Properties.h"

@interface CharacteristicTableViewCell ()

@end

@implementation CharacteristicTableViewCell

@synthesize switchControl;
@synthesize characteristic;
@synthesize sliderControl;
@synthesize segmentControl;
@synthesize typeLabel;
@synthesize valueLabel;

//
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        switchControl = [[UISwitch alloc] initWithFrame:CGRectMake(280, 26, 10, 10)];
        [switchControl addTarget:self action:@selector(didChangeSwitchValue:) forControlEvents:UIControlEventValueChanged];
        [self.contentView addSubview:switchControl];
        
        segmentControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(20, 30, 300, 30)];
        [segmentControl addTarget:self action:@selector(segmentedControlDidChange:) forControlEvents: UIControlEventValueChanged];
        segmentControl.tintColor = BACK_GROUND_HEAD_COLOR;
        [self.contentView addSubview:segmentControl];
        
        sliderControl = [[UISlider alloc] initWithFrame:CGRectMake(100.0, 34.0, 200.0, 10.0)] ;
        [sliderControl addTarget:self action:@selector(didChangeSliderValue:) forControlEvents:UIControlEventValueChanged];
        sliderControl.continuous=YES;
        [sliderControl setThumbImage:[UIImage imageNamed:@"ThumbImage"] forState:UIControlStateNormal];
        [self.contentView addSubview:sliderControl];
        
        typeLabel = [[UILabel alloc] init];
        [typeLabel setFrame:CGRectMake(20, 6, 300, 20)];
        [typeLabel setFont:[UIFont systemFontOfSize:17.0]];
        [typeLabel setTextColor:[UIColor grayColor]];
        [self.contentView addSubview:typeLabel];
                 
        valueLabel = [[UILabel alloc] init];
        [valueLabel setFrame:CGRectMake(20, 30, 280, 20)];
        [self.contentView addSubview:valueLabel];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private Methods
#pragma mark UI Action Methods
//Switch Functionality

- (void)didChangeSwitchValue:(UISwitch *)sender {
    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] && [self.delegate respondsToSelector:@selector(characteristicCell:didUpdateValue:forCharacteristic:)]) {
        [self.delegate characteristicCell:self didUpdateValue:@(sender.on) forCharacteristic:self.characteristic];
    }
}

//Slider Functionality
/**
 *  Responds to a change in slider value and alerts its delegate appropriately.
 *
 *  @param sender The slider which updated its value.
 */
- (void)didChangeSliderValue:(UISlider *)sender {
    id newValue = [self roundedValueForSliderValue:sender.value];    
    [self.valueLabel setText:[characteristic hmc_localizedDescriptionForValue:newValue]];

    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] && [self.delegate respondsToSelector:@selector(characteristicCell:didUpdateValue:forCharacteristic:)]) {
        [self.delegate characteristicCell:self didUpdateValue:@(sender.value) forCharacteristic:self.characteristic];
    }
}


//Segmented Control Functionality

- (void)segmentedControlDidChange:(UISegmentedControl *)sender {

    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] && [self.delegate respondsToSelector:@selector(characteristicCell:didUpdateValue:forCharacteristic:)]) {
        [self.delegate characteristicCell:self didUpdateValue:@(sender.selectedSegmentIndex) forCharacteristic:self.characteristic];
    }
}

#pragma mark MISC Methods

- (void)updatedValue {
    
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CharacteristicCellDelegate)] && [self.delegate respondsToSelector:@selector(characteristicCell:readInitialValueForCharacteristic:completion:)]) {
        [self.delegate characteristicCell:self readInitialValueForCharacteristic:characteristic completion:^(id value, NSError *error) {
            [self.valueLabel setText:[characteristic hmc_localizedDescriptionForValue:value]];
            
            if (characteristic.hmc_isBoolean) {
                // Set Switch Control State
                [self.switchControl setOn:[value boolValue]];
            } else if (characteristic.hmc_hasPredeterminedValueDescriptions) {
                // Set Segment Control Selected Index
                self.segmentControl.selectedSegmentIndex = [value integerValue];
            } else if (characteristic.hmc_isNumeric) {
                // Set Slider Control
                [self.sliderControl setValue:[value floatValue]];
            }
        }];
    }
}

/**
 *  @discussion Restricts a value to the step value provided in the cell's
 *  characteristic's metadata.
 *
 *  @param sliderValue The provided value.
 *
 *  @return The value adjusted to align with a step value.
 */
- (NSNumber *)roundedValueForSliderValue:(float)value {
    if (!self.characteristic.metadata.stepValue || [self.characteristic.metadata.stepValue isEqualToNumber:@0]) {
        return @(value);
    }
    double stepValue = self.characteristic.metadata.stepValue.doubleValue;
    double newStep = round(value / stepValue);
    double stepped = newStep * stepValue;
    return @(stepped);
}

@end
