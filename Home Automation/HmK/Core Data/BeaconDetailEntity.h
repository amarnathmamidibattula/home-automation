//
//  BeaconDetailEntity.h
//  Home Automation
//
//  Created by Santosh on 29/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RoomDetailEntity;

@interface BeaconDetailEntity : NSManagedObject

@property (nonatomic, retain) NSString * beaconID;
@property (nonatomic, retain) NSString * beaconName;
@property (nonatomic, retain) NSDate * index;
@property (nonatomic, retain) NSNumber * major;
@property (nonatomic, retain) NSNumber * minor;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSNumber * isBeaconDetected;
@property (nonatomic, retain) RoomDetailEntity *roomDetail;

@end
