//
//  BeaconDetailEntity.m
//  Home Automation
//
//  Created by Santosh on 29/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "BeaconDetailEntity.h"
#import "RoomDetailEntity.h"


@implementation BeaconDetailEntity

@dynamic beaconID;
@dynamic beaconName;
@dynamic index;
@dynamic major;
@dynamic minor;
@dynamic uuid;
@dynamic isBeaconDetected;
@dynamic roomDetail;

@end
