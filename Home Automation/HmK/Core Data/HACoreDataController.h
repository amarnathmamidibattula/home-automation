//
//  HACoreDataController.h
//  EventManagement
//
//  Created by Santosh on 19/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

/* 
 'HACoreDataController' class is singleton class. It is responsible for handling all the core data operation such as Create / Store / Retrieve / Delete manage object instance.
 */
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface HACoreDataController : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/* 
 Create the singleton object of 'HACoreDataController' class.
 */
+ (instancetype)sharedInstance;
/* 
 Method is used for save manage object context.
 */
- (void)saveContext;
/* 
 * Method is used for fetch the URL path from Documents directory
 * 
 * @return Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory;
/*
 * This Method is used for creating Manage object instance.
 *
 * @return Returns manage object instance. 
 */
- (NSManagedObject *)createManageObjectWithName:(NSString *)entityName;
/*
 * Fetch all the manage objects for the |entityName|
 *
 * @params entityName   for which entity manage objects need to be feteched.
 * @params predicate    for filtered 
 * @params sortkey      for which attibutes manage objects need to be sorted.
 *
 * @return Returns the list of manage objects from core data.
 */
- (NSArray *)fetchManageObjectsWithName:(NSString *)entityName
                              predicate:(NSPredicate *)predicate
                                sortKey:(NSString *)sortKey;
@end
