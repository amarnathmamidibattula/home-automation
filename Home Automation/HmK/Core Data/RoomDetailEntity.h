//
//  RoomDetailEntity.h
//  Home Automation
//
//  Created by Santosh on 07/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BeaconDetailEntity;

@interface RoomDetailEntity : NSManagedObject

@property (nonatomic, retain) NSString * roomID;
@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) BeaconDetailEntity *beaconDetail;

@end
