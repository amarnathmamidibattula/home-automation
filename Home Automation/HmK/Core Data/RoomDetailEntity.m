//
//  RoomDetailEntity.m
//  Home Automation
//
//  Created by Santosh on 07/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "RoomDetailEntity.h"
#import "BeaconDetailEntity.h"


@implementation RoomDetailEntity

@dynamic roomID;
@dynamic roomName;
@dynamic beaconDetail;

@end
