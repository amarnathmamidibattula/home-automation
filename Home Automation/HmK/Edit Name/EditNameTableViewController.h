//
//  EditName.h
//  HmK
//
//  Created by Santosh on 23/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

/**
 * @enum This is a thing.
 */
typedef NS_ENUM(NSUInteger, EditNameType) {

    EditNameTypeHome = 0,
    EditNameTypeRoom,
    EditNameTypeZone
};

@interface EditNameTableViewController : UITableViewController

@property (nonatomic, strong) HMHome *editHome;
@property (nonatomic, strong) HMRoom *editRoom;
@property (nonatomic, strong) HMZone *editZone;
@property (nonatomic, strong) HMHome *selectedHome;
@property (nonatomic, assign) EditNameType editNameType;
@end
