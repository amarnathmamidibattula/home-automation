//
//  EditName.m
//  HmK
//
//  Created by Santosh on 23/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];

#import "EditNameTableViewController.h"

@interface EditNameTableViewController () <UITextFieldDelegate, HMHomeDelegate>

@property (nonatomic, weak) IBOutlet UITableView *editNameTableView ;
@property (nonatomic) IBOutlet UITextField *nameField;

@end

@implementation EditNameTableViewController

@synthesize editNameTableView;
@synthesize nameField;
@synthesize editHome;
@synthesize editRoom;
@synthesize editZone;
@synthesize selectedHome;
@synthesize editNameType;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.editNameTableView.estimatedRowHeight = 44.0;
    self.editNameTableView.rowHeight = UITableViewAutomaticDimension;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save:)];
    self.navigationController.view.backgroundColor =
    [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackGround"]];
    
    
    switch (editNameType) {
        case EditNameTypeHome:
        {
            nameField.text=editHome.name;
            [self.navigationItem setTitle:editHome.name];
        }
            break;
            
        case EditNameTypeRoom:
        {
            nameField.text=editRoom.name;
            [self.navigationItem setTitle:selectedHome.name];
        }
            break;
            
        case EditNameTypeZone:
        {
            nameField.text=editZone.name;
            [self.navigationItem setTitle:selectedHome.name];
        }
            break;
            
        default:
            break;
            
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)save: (id)sender{
    switch (editNameType) {
        case EditNameTypeHome:
        {
            [self update:self.nameField.text forHome:self.editHome];
            
        }
            break;
            
        case EditNameTypeRoom:
        {
            [self update:self.nameField.text forRoom:self.editRoom];
        }
            break;
            
        case EditNameTypeZone:
        {
            [self update:self.nameField.text forZone:self.editZone];
        }
            break;
    }
}


- (void)update:(NSString *)name forHome:(HMHome *)home {
    
    NSLog(@"The home name is: %@", name);
    if ([home.name isEqualToString:name]) {
        return;
    }
    [home updateName:name completionHandler:^(NSError *error) {
        if (error) {
        }
    }];
}

- (void)update:(NSString *)name forRoom:(HMRoom *)room {
    
    NSLog(@"The room name is: %@", name);
    if ([room.name isEqualToString:name]) {
        return;
    }
    [room updateName:name completionHandler:^(NSError *error) {
        if (error) {
        }
    }];
}

- (void)update:(NSString *)name forZone:(HMZone *)zone {
    
    NSLog(@"The zone name is: %@", name);
    if ([zone.name isEqualToString:name]) {
        return;
    }
    [zone updateName:name completionHandler:^(NSError *error) {
        if (error) {
        }
    }];
}


//Header Title
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    {
    switch (editNameType) {
        case EditNameTypeHome:
        {
            return [NSString stringWithFormat:@"Edit Home Name:"];
            
        }
            break;
            
        case EditNameTypeRoom:
        {
            return [NSString stringWithFormat:@"Edit Room Name:"];
        }
            break;
            
        case EditNameTypeZone:
        {
            return [NSString stringWithFormat:@"Edit Zone Name:"];
        }
            break;
    }
}

//Footer Title
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    return [NSString stringWithFormat:@"Click on 'Save' to save name"];
}

#pragma mark - UITextfield Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // return NO to disallow editing.
    NSLog(@"textFieldShouldBeginEditing :%@",textField.text);
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    // became first responder
    NSLog(@"textFieldDidBeginEditing :%@",textField.text);
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
{
    NSLog(@"textFieldShouldEndEditing :%@",textField.text);
    
    return YES;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
    NSLog(@"textFieldDidEndEditing :%@",textField.text);
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    NSLog(@"shouldChangeCharactersInRange :%@",textField.text);
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // called when clear button pressed. return NO to ignore (no notifications)
    NSLog(@"textFieldShouldClear :%@",textField.text);
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    NSLog(@"textFieldShouldReturn :%@",textField.text);
    [textField resignFirstResponder];
    
    return YES;
    
}

@end
