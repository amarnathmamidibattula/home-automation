//
//  EditTableViewCell.h
//  Home Automation
//
//  Created by Amarnath on 9/1/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *editTextField;

@end
