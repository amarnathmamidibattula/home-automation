//
//  ViewController.h
//  HmK
//
//  Created by Santosh on 10/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF HOMES ADDED AND PROVIDE OPTION TO ENTER NEW HOME



#import <UIKit/UIKit.h>
@import HomeKit;
@interface HomeKitViewController  : UIViewController <HMHomeManagerDelegate, UITableViewDelegate, UITableViewDataSource>
{
    
}
@property (nonatomic, weak) IBOutlet UITableView *homeKitTable ; //Displays List of Homes

@end

