//
//  ViewController.m
//  HmK
//
//  Created by Santosh on 10/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF HOMES ADDED AND PROVIDE OPTION TO ENTER NEW HOME

#import "HomeKitViewController.h"
#import "RoomViewController.h"
#import "EditNameTableViewController.h"
#import "CloudkitController.h"
#import "HAHomeManager.h"
#import "CloudkitParams.h"
#import "HomeProgramTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];

typedef NS_ENUM(NSUInteger, HomeTableViewSection) {
    HomeTableViewSectionHomeName = 0,
    HomeTableViewSectionPrimaryHome,
    HomeTableViewSectionAddBeacon,
    HomeTableViewSectionTotalCount
};

@interface HomeKitViewController ()
{
    HMHomeManager * homeManager;
}

@property (nonatomic,strong) HMHomeManager * homeManager;

@end

@implementation HomeKitViewController

@synthesize homeKitTable;
@synthesize homeManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.homeManager = [[HAHomeManager manager] homeManager];
    self.homeManager.delegate = self;
    self.homeKitTable.layer.cornerRadius = 10;
    self.homeKitTable.layer.masksToBounds = YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
#pragma mark Button Action
// To enter name of home
- (void)buttonNameClicked:(UIButton*)sender {

    NSString *title = NSLocalizedString(@"Home", nil);
    NSString *message = NSLocalizedString(@"Enter Home Name", nil);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    // Add the text field for text entry.
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // If you need to customize the text field, you can do so here.
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    NSString *addString = [NSString stringWithFormat:@"Add"];
    
    UIAlertAction *addNewObject = [UIAlertAction actionWithTitle:addString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString  *newName = [alertController.textFields.firstObject text];
        NSString  *trimmedName = [newName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // If the user didn't input anything useful,
        // don't even bother with the completion handler.
        if (trimmedName.length > 0) {
        [self addHome:trimmedName];
        }
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:addNewObject];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (IBAction)onClcikInfo:(id)sender {
    [UIView setAnimationsEnabled:NO];
    self.view.hidden = YES;
    [self performSegueWithIdentifier:@"EditNameSegue" sender:sender];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView setAnimationsEnabled:YES];
        self.view.hidden = NO;
    });
    
}
- (IBAction)onClickAddButton:(id)sender {
    UIButton *button=(UIButton*)sender;
    if([button.titleLabel.text isEqualToString:@"Add Home"]){
        [self buttonNameClicked:nil];
    }else{
        [self performSegueWithIdentifier:@"AddBeaconSegue" sender:sender];
    }
}

#pragma mark Navigate
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([@"HomeToRoomSegue" isEqualToString:segue.identifier]) {

        NSIndexPath *indexPath = (NSIndexPath *)sender;
        HMHome *home = [self.homeManager.homes objectAtIndex:indexPath.row];
        RoomViewController *viewController = (RoomViewController *)[segue destinationViewController];
        viewController.selectedHome = home;
    } else if ([@"EditNameSegue" isEqualToString:segue.identifier]) {
        UIButton *button = (UIButton*)sender;
        NSInteger index = button.tag;
        HMHome *home = [self.homeManager.homes objectAtIndex:index];
        EditNameTableViewController *viewController= (EditNameTableViewController *)[segue destinationViewController];
        viewController.editHome = home;
        viewController.editNameType = EditNameTypeHome;
    } else if ([@"AddBeaconSegue" isEqualToString:segue.identifier]) {
        // Display Add beacon screen
    }
}

#pragma mark Add Home

- (void)addHome:(NSString *)name {
    
    __weak typeof(self) weakSelf = self; // If the capture of self is coming in with implicit property access of self.object - we can't refer to         self or properties on self from within a block that will be strongly retained by self. We can solve this by creating a weak reference to self before accessing object inside the block
    
    [self.homeManager addHomeWithName:name completionHandler:^(HMHome *newHome, NSError *error) {
        
        NSLog(@"Error%@",error );
        [weakSelf storeRoomDetail:name WithCompletionBlock:^(NSError *error) {
            
            [weakSelf.homeKitTable reloadData];
        }];
    }];
}

#pragma mark API Call

- (void)storeRoomDetail:(NSString *)roomName WithCompletionBlock:(void (^)(NSError *error))completion {
    
    // API Parameters
    NSDictionary *params = @{HACloudRoomIDAttributeName:[NSString stringWithFormat:@"%@+%@",roomName,@"DefaultRoom"],
                             HACloudRoomNameAttributeName:@"DefaultRoom",
                             HACloudIndexAttributeName:[NSDate date]
                             };
    
    [[CloudkitController dataHandler] storeRoomDetailsOniCloud:params
                                           withCompletionBlock:^(CKRecord *roomRecord, NSError *error) {
                                                     if (!error) {
                                                         // Insert successfully saved room record code
                                                         
                                                         [[CloudkitController dataHandler] storeiCloudRoomDetail:roomRecord completion:^(RoomDetailEntity *roomDetailEntity) {
                                                             
                                                             completion (nil);
                                                         }];
                                                         
                                                     } else {
                                                         // Insert error handling
                                                         NSLog(@"beaconRecord error :%@",error);
                                                         completion (error);
                                                     }
                                                 }];
}

#pragma mark Update Primary Home
- (void)updatePrimaryHome:(HMHome *)newPrimaryHome {
    if (newPrimaryHome == self.homeManager.primaryHome) {
        return;
    }
    [self.homeManager updatePrimaryHome:newPrimaryHome completionHandler:^(NSError *error) {
        if (error) {
            return;
        }
        [self didUpdatePrimaryHome];
    }];
}

/**
 *  Reloads the primary home section to check the new primary home.
 */
- (void)didUpdatePrimaryHome {

    [self.homeKitTable beginUpdates];
    [self.homeKitTable reloadData];
    [self.homeKitTable endUpdates];
}

#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Default is 1 if not implemented
    return HomeTableViewSectionAddBeacon;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case HomeTableViewSectionHomeName: return ([self.homeManager.homes count] + 1/*Added for add new room*/) ;
        case HomeTableViewSectionPrimaryHome: return [self.homeManager.homes count]+1;
        case HomeTableViewSectionAddBeacon: return 1;
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *simpleTableIdentifier = @"HomeTableViewCell";
    HomeProgramTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    switch (indexPath.section) {
        case HomeTableViewSectionHomeName: {
            
            if ([self.homeManager.homes count]> indexPath.row) {
                // Display Home name
                cell.informationImage.tag=indexPath.row;
                [cell.informationImage setHidden:NO];
                [cell.homeImageView setHidden:NO];
                HMHome *home = [self.homeManager.homes objectAtIndex:indexPath.row];
                cell.homeName.text = [home valueForKey:@"name"];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                [cell.addImage setHidden:YES];
            } else {
                // Add new Action Set
                [cell.informationImage setHidden:YES];
                [cell.homeImageView setHidden:YES];
                [cell.addImage setHidden:NO];
                cell.homeName.text = @"Add Home...";
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        } break;
    
        case HomeTableViewSectionPrimaryHome: {
             [cell.informationImage setHidden:YES];
            if ([self.homeManager.homes count]> indexPath.row) {
            HMHome *home = [self.homeManager.homes objectAtIndex:indexPath.row];
            cell.homeName.text = [home valueForKey:@"name"];
            [cell.homeImageView setHidden:NO];
            [cell.addImage setHidden:YES];
            
            if (home == self.homeManager.primaryHome) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            }else{
                cell.homeName.text = @"Add Beacon ...";
                [cell.homeImageView setHidden:YES];
                [cell.addImage setHidden:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }break;
            
        default: break;
    }

    return cell;
}

//Header Title
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    // fixed font style. use custom view (UILabel) if you want something different
{
    switch (section) {
        case HomeTableViewSectionHomeName: return @"Home Name:";
        case HomeTableViewSectionPrimaryHome: return @"Select Your Primary Home:";
        default: return nil;
    }
}

//Footer Title
//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
//
//    switch (section) {
////        case 0: return @"Tap on home name to view/add accessory";
//        case HomeTableViewSectionPrimaryHome: return @"Selected home is the Primary Home";
//        default: return nil;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case HomeTableViewSectionHomeName: {
            if ([self.homeManager.homes count] > indexPath.row) {
                // Display home information
                [self performSegueWithIdentifier:@"HomeToRoomSegue" sender:indexPath];
            } else {
                // Add new home
                [self buttonNameClicked:nil];
            }
        } break;
            
        case HomeTableViewSectionPrimaryHome: {
            if ([self.homeManager.homes count] > indexPath.row) {
            HMHome *newPrimaryHome = self.homeManager.homes[indexPath.row];
                [self updatePrimaryHome:newPrimaryHome];
            }else{
                [self performSegueWithIdentifier:@"AddBeaconSegue" sender:indexPath];
            }
        } break;
        
        default: break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//To delete Home Name
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case HomeTableViewSectionHomeName: {
            HMHome *home = [self.homeManager.homes objectAtIndex:indexPath.row];
            [self.homeManager removeHome:home completionHandler:^(NSError *error) {
                NSLog(@"error :%@ ",error);
                [self.homeKitTable reloadData];
            }];

        } break;

        default: break;
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case HomeTableViewSectionHomeName: [self performSegueWithIdentifier:@"EditNameSegue" sender:indexPath];
                break;

        default: break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
//        case 0: return 40.0;
        case HomeTableViewSectionPrimaryHome: return 30.0;
        default: return 0.0f;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = BACK_GROUND_HEAD_COLOR;
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
        
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    switch (section) {
//        case 0: {
//            UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 44.0)] ;
//            
//            //label
//            UILabel*    footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 250, 30)];
//            footerLabel.backgroundColor = [UIColor clearColor];
//            footerLabel.text = @"Click here to add new Home ";
//            footerLabel.textColor = [UIColor blackColor];
//            [footerView addSubview: footerLabel];
//            
//            //button
//            UIButton *buttonName;
//            buttonName = [UIButton buttonWithType:UIButtonTypeCustom];
//            [buttonName setFrame:CGRectMake(250, 5, 80, 30)];
//            [buttonName setTitle:@"Add" forState:UIControlStateNormal];
//            [buttonName setBackgroundColor:[UIColor darkGrayColor]];
//            [buttonName addTarget:self action:@selector(buttonNameClicked:) forControlEvents:UIControlEventTouchUpInside];
//            [footerView addSubview:buttonName];
//            
//            return footerView;
//        }
//        case 1: return nil;
        default: return nil;
    }
}

#pragma mark HMHomeManager Delegate Methods

- (void)homeManagerDidUpdateHomes:(HMHomeManager *)manager {
        
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.homeKitTable reloadData];
    });
}

- (void)homeManager:(HMHomeManager *)manager didAddHome:(HMHome *)home {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.homeKitTable reloadData];
    });
}

- (void)homeManager:(HMHomeManager *)manager didRemoveHome:(HMHome *)home {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.homeKitTable reloadData];
    });
}

- (void)homeManagerDidUpdatePrimaryHome:(HMHomeManager *)manager {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.homeKitTable reloadData];
    });
//    [self didUpdatePrimaryHome];
}

@end
