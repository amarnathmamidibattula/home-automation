//
//  HomeProgramTableViewCell.h
//  Home Automation
//
//  Created by Amarnath on 8/26/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeProgramTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *homeImageView;
@property (weak, nonatomic) IBOutlet UILabel *homeName;
@property (weak, nonatomic) IBOutlet UIButton *addHome;
@property (weak, nonatomic) IBOutlet UIButton *informationImage;
@property (weak, nonatomic) IBOutlet UIButton *addImage;

@end
