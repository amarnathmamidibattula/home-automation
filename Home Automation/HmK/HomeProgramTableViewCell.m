//
//  HomeProgramTableViewCell.m
//  Home Automation
//
//  Created by Amarnath on 8/26/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "HomeProgramTableViewCell.h"

@implementation HomeProgramTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.addHome.layer.cornerRadius = 5.0f;
    [self.addHome.layer setBorderWidth:1.0f];
    self.addHome.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
