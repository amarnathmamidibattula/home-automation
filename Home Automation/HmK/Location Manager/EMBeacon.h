//
//  EMBeacon.h
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;
/* 
 * 'EMBeacon' class contain all the information about Beacon such as 
 *  UUID String, Major, Minor Value and its Identifier.
 */

@interface EMBeacon : NSObject

@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSUUID *uuid;
@property (assign, nonatomic, readonly) CLBeaconMajorValue majorValue;
@property (assign, nonatomic, readonly) CLBeaconMinorValue minorValue;

/**
 *  Create 'EMBeacon' instance which contains all the information related to that beacon.
 *
 *  @param name  Beacon Name
 *  @param uuid  Universally Unique Identifier (16-bytes)
 *  @param major Major (2 bytes) and
 *  @param minor Minor (2 bytes)
 *
 *  @return Returns 'EMBeacon' instance
 */
- (instancetype)initWithName:(NSString *)name
                        uuid:(NSUUID *)uuid
                       major:(CLBeaconMajorValue)major
                       minor:(CLBeaconMinorValue)minor;

@end
