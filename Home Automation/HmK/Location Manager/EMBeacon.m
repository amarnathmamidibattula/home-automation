//
//  EMBeacon.m
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

#import "EMBeacon.h"

@implementation EMBeacon

@synthesize name;
@synthesize uuid;
@synthesize majorValue;
@synthesize minorValue;

- (instancetype)initWithName:(NSString *)_name
                        uuid:(NSUUID *)_uuid
                       major:(CLBeaconMajorValue)major
                       minor:(CLBeaconMinorValue)minor {
    self = [super init];
    if (self) {
        name = _name;
        uuid = _uuid;
        majorValue = major;
        minorValue = minor;
    }
    
    return self;
}

@end
