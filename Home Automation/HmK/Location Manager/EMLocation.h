//
//  EMLocation.h
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface EMLocation : NSObject

@property (strong, nonatomic, readonly) NSString *locationName;
@property (assign, nonatomic, readonly) CLLocationDegrees latitude;
@property (assign, nonatomic, readonly) CLLocationDegrees longitude;
@property (assign, nonatomic, readonly) CLLocationDistance radius;

/**
 *  Initialize 'EMLocation' instance for Geo-fencing location.
 *
 *  @param name      location name
 *  @param latitude  location latitude
 *  @param longitude location longitude
 *  @param radius    location radius in meters
 *
 *  @return Returns 'EMLocation' instance.
 */
- (instancetype)initWithLocationName:(NSString *)name
                            latitude:(CLLocationDegrees)latitude
                           longitude:(CLLocationDegrees)longitude
                              radius:(CLLocationDistance)radius;
@end
