//
//  EMLocation.m
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

#import "EMLocation.h"

@implementation EMLocation

@synthesize locationName;
@synthesize latitude;
@synthesize longitude;
@synthesize radius;

- (instancetype)initWithLocationName:(NSString *)name
                            latitude:(CLLocationDegrees)_latitude
                           longitude:(CLLocationDegrees)_longitude
                              radius:(CLLocationDistance)_radius {
    self = [super init];
    if (self) {
        locationName = name;
        latitude = _latitude;
        longitude = _longitude;
        radius = _radius;
    }
    
    return self;
}

@end
