//
//  EMLocationManager.h
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@import CoreLocation;
@class EMLocationManager, EMBeacon, EMLocation;

@protocol EMLocationManagerDelegate <NSObject>

- (void)location:(EMLocationManager *)manager didEnterRegion:(CLRegion *)region;
- (void)location:(EMLocationManager *)manager didExitRegion:(CLRegion *)region;
- (void)location:(EMLocationManager *)manager didRangeBeacons:(NSArray*)beacons inRegion:(CLBeaconRegion*)region;
- (void)location:(EMLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error;

@end
/**
 *  'EMLocationManager' class handle all the location based operation such as
 *  looking for current location start monitoring for region and start/stop ranging for beacons.
 */
@interface EMLocationManager : NSObject

@property (nonatomic, weak) id delegate;
/**
 *  Method used for start monitoring for circular region
 *
 *  @param location 'EMLocation' instance
 */
- (void)startMonitoringForCircularRegion:(EMLocation *)location;
/**
 *  Method designed for start looking for targeted beacon
 *
 *  @param region beacon region
 */
- (void)startRangingBeaconsInRegion:(CLBeaconRegion *)region;
/**
 *  Method designed for stop looking for target beacon
 *
 *  @param region beacon region 
 */
- (void)stopRangingBeaconsInRegion:(CLBeaconRegion *)region;
@end
