//
//  EMLocationManager.m
//  EventManagement
//
//  Created by Santosh on 27/11/14.
//  Copyright (c) 2014 Santosh. All rights reserved.
//

static const double DEFAULT_DISTANCE_FILTER = 50.00;

#import "EMLocationManager.h"
#import "HAConfiguration.h"
#import <objc/runtime.h>
#import "EMBeacon.h"
#import "EMLocation.h"

@interface EMLocationManager ()<CLLocationManagerDelegate>
{
    dispatch_queue_t geofenceQueue;
    dispatch_queue_t beaconQueue;
	void *geofenceQueueTag;
	void *beaconQueueTag;
}

@property (nonatomic, strong) CLLocationManager *locationManager;

/**
 *  This method asynchronously invokes the given block (dispatch_async) on the geofenceQueue.
 *
 * This method is designed to be invoked from within the geofencing and check whether bluetooth is enabled or not.
 * In other words, it is expecting to be invoked from a dispatch_queue other than the geofenceQueue.
 * If you attempt to invoke this method from within the geofenceQueue, an exception is thrown.
 * Therefore care should be taken when designing your implementation.
 *
 */
- (void)executeGeofenceBlock:(dispatch_block_t)block;
/**
 *  This method asynchronously invokes the given block (dispatch_async) on the beaconQueue.
 *
 * This method is designed to be invoked for recognized beacon in the region.
 * In other words, it is expecting to be invoked from a dispatch_queue other than the beaconQueue.
 * If you attempt to invoke this method from within the beaconQueue, an exception is thrown.
 * Therefore care should be taken when designing your implementation.
 *
 */
- (void)executeBeaconBlock:(dispatch_block_t)block;
/**
 *  Initialize Location Manager
 */
- (void)initializeLocationManager;
/**
 *  This method is used to create 'CLBeaconRegion' instance from 'EMBeacon' instance
 *
 *  @param beacon 'EMBeacon' instance
 *
 *  @return Returns the 'CLBeaconRegion' instance.
 */
- (CLBeaconRegion *)beaconRegionWithBeaconItem:(EMBeacon *)beacon;
/**
 *  create 'CLCircularRegion' instance
 *
 *  @param location contains latitude, longitude and location name
 *
 *  @return Returns the 'CLCircularRegion' instance.
 */
- (CLCircularRegion *)circularRegionWithLocation:(EMLocation *)location;
@end

@implementation EMLocationManager

@synthesize delegate;
@synthesize locationManager;
//@synthesize myBeaconRegion;

- (instancetype)init {
    self = [super init];
    if (self) {
        // Private queue for geofencing
        geofenceQueue = dispatch_queue_create("com.tcs.event.geofence", NULL);
		geofenceQueueTag = &geofenceQueue;
		dispatch_queue_set_specific(geofenceQueue, geofenceQueueTag, geofenceQueueTag, NULL);
        // Private queue for beacon recognization
        beaconQueue = dispatch_queue_create("com.tcs.event.beacon", NULL);
        beaconQueueTag = &beaconQueue;
		dispatch_queue_set_specific(beaconQueue, beaconQueueTag, beaconQueueTag, NULL);

        // Initialize CLLocationManager instance
        [self initializeLocationManager];
    }
    
    return self;
}

- (void)dealloc {
   // self.locationManager.delegate = nil;
   // self.locationManager = nil;
}

#pragma mark - Private Method

- (void)executeGeofenceBlock:(dispatch_block_t)block {
	// By design this method should not be invoked from the geofenceQueue.
	//
	// If you remove the assert statement below, you are destroying the sole purpose for this class,
	// which is to optimize the disk IO by buffering save operations.
	//
	NSAssert(!dispatch_get_specific(geofenceQueueTag), @"Invoked on incorrect queue");
    dispatch_async(geofenceQueue, ^{ @autoreleasepool {
        block();
        
    }});
}

- (void)executeBeaconBlock:(dispatch_block_t)block {
	// By design this method should not be invoked from the geofenceQueue.
	//
	// If you remove the assert statement below, you are destroying the sole purpose for this class,
	// which is to optimize the disk IO by buffering save operations.
	//
	NSAssert(!dispatch_get_specific(beaconQueueTag), @"Invoked on incorrect queue");
    dispatch_async(beaconQueue, ^{ @autoreleasepool {
        block();
        
    }});
}

- (void)initializeLocationManager {

    // Initialize location manager and set ourselves as the delegate
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.distanceFilter = DEFAULT_DISTANCE_FILTER;
    // New property for iOS6
    if ([self.locationManager respondsToSelector:@selector(activityType)]) {
        self.locationManager.activityType = CLActivityTypeFitness;
    }
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];

    // Check to ensure location services are enabled
    if(![CLLocationManager locationServicesEnabled]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You need to enable location services to use this app."
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

- (CLBeaconRegion *)beaconRegionWithBeaconItem:(EMBeacon *)beacon {

    NSAssert(dispatch_get_specific(beaconQueueTag), @"Invoked on incorrect queue");

    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beacon.uuid
                                                                           major:beacon.majorValue
                                                                           minor:beacon.minorValue
                                                                      identifier:beacon.name];
    return beaconRegion;
}

- (CLCircularRegion *)circularRegionWithLocation:(EMLocation *)location {

    NSAssert(dispatch_get_specific(geofenceQueueTag), @"Invoked on incorrect queue");

    CLLocationDegrees latitude = location.latitude;
    CLLocationDegrees longitude = location.longitude;
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    return [[CLCircularRegion alloc] initWithCenter:centerCoordinate
                                             radius:location.radius
                                         identifier:location.locationName];
}

#pragma mark - Public Method

- (void)startMonitoringForCircularRegion:(EMLocation *)location  {
    
    [self executeGeofenceBlock:^{
        // Setup circular region for Geo-fencing.
        CLCircularRegion *circularRegion = [self circularRegionWithLocation:location];
        circularRegion.notifyOnExit = YES;
        circularRegion.notifyOnEntry = YES;
        // Tell location manager to start monitoring for circular region
        [self.locationManager startMonitoringForRegion:circularRegion];
        // Check if circular monitoring is available for this device
        if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Display Alert Message
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Monitoring not available"
                                                                message:nil
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles: nil];
                [alert show];
            });
        }
    }];
}

- (void)startRangingBeaconsInRegion:(CLBeaconRegion *)region
{
    [self executeBeaconBlock:^{
        // We entered a region, now start looking for our target beacons!
        [self.locationManager startRangingBeaconsInRegion:region];
    }];
}

- (void)stopRangingBeaconsInRegion:(CLBeaconRegion *)region
{
    // We exited a region, stop looking for our target beacons
    [self.locationManager stopRangingBeaconsInRegion:region ];
}

#pragma mark - Delegate Method
#pragma mark CLLocationManagerDelegate Protocols
- (void)locationManager:(CLLocationManager *)manager
        didStartMonitoringForRegion:(CLRegion *)region {
    //[self executeGeofenceBlock:^{
        // Request state for region , Call locationManager:didDetermineState:forRegion: delegate method
        [self.locationManager requestStateForRegion:region];
    //}];
}

- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion *)region {
}

- (void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion *)region {
}

- (void)locationManager:(CLLocationManager *)manager
        didDetermineState:(CLRegionState)state
                forRegion:(CLRegion *)region {
    DebugLog(@"region state :%i",state);

    switch (state) {
        case CLRegionStateInside:
        case CLRegionStateOutside:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.delegate && [self.delegate conformsToProtocol:@protocol(EMLocationManagerDelegate)]
                    && [self.delegate respondsToSelector:@selector(location:didEnterRegion:)]) {
                    [self.delegate location:self didEnterRegion:region];
                }
            });
        }
            break;
//        case CLRegionStateOutside:
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (self.delegate && [self.delegate conformsToProtocol:@protocol(EMLocationManagerDelegate)]
//                    && [self.delegate respondsToSelector:@selector(location:didExitRegion:)]) {
//                    [self.delegate location:self didExitRegion:region];
//                }
//            });
//        }
//            break;
        case CLRegionStateUnknown:
        {
            
        }
            break;
            
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager*)manager
        didRangeBeacons:(NSArray*)beacons
               inRegion:(CLBeaconRegion*)region {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate conformsToProtocol:@protocol(EMLocationManagerDelegate)]
            && [self.delegate respondsToSelector:@selector(location:didRangeBeacons:inRegion:)]) {
            [self.delegate location:self didRangeBeacons:beacons inRegion:region];
        }
    });
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations firstObject];
    DebugLog(@"current location latitude :%f longitude :%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
}

- (void)locationManager:(CLLocationManager *)manager
        rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
                             withError:(NSError *)error {
    //DebugLog(@"rangingBeaconsDidFailForRegion : %@ \n error: %@", region, error);
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    //DebugLog(@"locationManager didFailWithError : %@", error);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate conformsToProtocol:@protocol(EMLocationManagerDelegate)]
            && [self.delegate respondsToSelector:@selector(location:monitoringDidFailForRegion:withError:)]) {
            [self.delegate location:self monitoringDidFailForRegion:nil withError:error];
        }
    });
}

- (void)locationManager:(CLLocationManager *)manager
        monitoringDidFailForRegion:(CLRegion *)region
                         withError:(NSError *)error {
    //DebugLog(@"monitoringDidFailForRegion : %@", error);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.delegate && [self.delegate conformsToProtocol:@protocol(EMLocationManagerDelegate)]
            && [self.delegate respondsToSelector:@selector(location:monitoringDidFailForRegion:withError:)]) {
            [self.delegate location:self monitoringDidFailForRegion:region withError:error];
        }
    });
}

@end
