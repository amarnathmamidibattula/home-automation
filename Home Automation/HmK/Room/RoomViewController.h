//
//  RoomViewController.h
//  HmK
//
//  Created by Santosh on 17/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF ROOMS ADDED TO A HOME AND OPTION TO ADD NEW ROOMS

#import <UIKit/UIKit.h>
@import HomeKit;

@interface RoomViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *roomListTableView ; //Displays List of Homes
@property (nonatomic, strong) HMHome *selectedHome;
@property (nonatomic, strong) HMRoom *room;

@end
