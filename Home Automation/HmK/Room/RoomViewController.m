//
//  RoomViewController.m
//  HmK
//
//  Created by Santosh on 17/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF ROOMS ADDED TO A HOME AND OPTION TO ADD NEW ROOMS

#define EDIT_NAME_SEGUE                 @"EditNameSegue"
#define ADD_ACTIONSET_SEGUE             @"AddNewActionSetSegue"
#define TIMER_TRIGGER_SEGUE             @"TimerTriggerSegue"
#define BACK_GROUND_HEAD_COLOR [UIColor colorWithRed:109.0/225.0 green:200.0/225.0 blue:255.0/225.0 alpha:1.0];


#import "RoomViewController.h"
#import "EditNameTableViewController.h"
#import "AccessoryViewController.h"
#import "ZoneRoomViewController.h"
#import "ActionSetViewController.h"
#import "TimerTriggerViewController.h"
#import "CloudkitController.h"
#import "CloudkitParams.h"
#import "RoomTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSUInteger, TableViewSection) {
    TableViewSectionForRoom = 0,
    TableViewSectionForZone,
    TableViewSectionForActionSet,
    TableViewSectionForTrigger,
    TableViewTotalSectionCount
};

@interface RoomViewController () <HMHomeDelegate, HMAccessoryDelegate, UITableViewDataSource, UITableViewDelegate>

@end

@implementation RoomViewController

@synthesize roomListTableView;
@synthesize selectedHome;

//viewDidLoad to be changed accordingly
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:selectedHome.name];
    self.roomListTableView.layer.cornerRadius = 10;
     self.roomListTableView.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.roomListTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)addRoom:(NSString *)name {
    __weak typeof(self) weakSelf = self;
    [self.selectedHome addRoomWithName:name completionHandler:^(HMRoom *room, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [weakSelf storeRoomDetail:name WithCompletionBlock:^(NSError *error) {
            
            [weakSelf.roomListTableView reloadData];
        }];
    }];
}

- (void)addZone:(NSString *)name {
    __weak typeof(self) weakSelf = self;
    [self.selectedHome addZoneWithName:name completionHandler:^(HMZone *zone, NSError *error) {
        NSLog(@"Error%@",error );
        NSLog(@"Zone: %@", zone.name);
        [weakSelf.roomListTableView reloadData];
    }];
}
- (IBAction)onClickInfo:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.roomListTableView];
    NSIndexPath *clickedIP = [self.roomListTableView indexPathForRowAtPoint:buttonPosition];
    if(clickedIP.section==TableViewSectionForRoom || clickedIP.section==TableViewSectionForZone){
        [UIView setAnimationsEnabled:NO];
        self.view.hidden = YES;
        [self tableView:self.roomListTableView accessoryButtonTappedForRowWithIndexPath:clickedIP];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView setAnimationsEnabled:YES];
            self.view.hidden = NO;
        });

    }else{
        [self tableView:self.roomListTableView accessoryButtonTappedForRowWithIndexPath:clickedIP];
    }
}

#pragma mark API Call

- (void)storeRoomDetail:(NSString *)roomName WithCompletionBlock:(void (^)(NSError *error))completion {
    
    // API Parameters
    NSDictionary *params = @{HACloudRoomIDAttributeName:[NSString stringWithFormat:@"%@+%@",self.selectedHome.name,roomName],
                             HACloudRoomNameAttributeName:roomName,
                             HACloudIndexAttributeName:[NSDate date]
                             };
    
    [[CloudkitController dataHandler] storeRoomDetailsOniCloud:params
                                           withCompletionBlock:^(CKRecord *roomRecord, NSError *error) {
                                               if (!error) {
                                                   // Insert successfully saved room record code
                                                   
                                                   [[CloudkitController dataHandler] storeiCloudRoomDetail:roomRecord completion:^(RoomDetailEntity *roomDetailEntity) {
                                                       
                                                       completion (nil);
                                                   }];
                                                   
                                               } else {
                                                   // Insert error handling
                                                   NSLog(@"beaconRecord error :%@",error);
                                                   completion (error);
                                               }
                                           }];
}

#pragma mark UIButton Action
// To enter name of room
- (void)onAddRoomButtonPress:(UIButton*)sender {
    
    NSString *title = NSLocalizedString(@"Room", nil);
    NSString *message = NSLocalizedString(@"Enter Room Name", nil);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    // Add the text field for text entry.
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // If you need to customize the text field, you can do so here.
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    NSString *addString = [NSString stringWithFormat:@"Add"];
    
    UIAlertAction *addNewObject = [UIAlertAction actionWithTitle:addString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString  *newName = [alertController.textFields.firstObject text];
        NSString  *trimmedName = [newName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // If the user didn't input anything useful,
        // don't even bother with the completion handler.
        if (trimmedName.length > 0) {
           [self addRoom:trimmedName];
        }
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:addNewObject];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)onAddZoneButtonPress:(UIButton *)sender {
    NSString *title = NSLocalizedString(@"Zone", nil);
    NSString *message = NSLocalizedString(@"Enter Zone Name", nil);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    // Add the text field for text entry.
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        // If you need to customize the text field, you can do so here.
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    NSString *addString = [NSString stringWithFormat:@"Add"];
    
    UIAlertAction *addNewObject = [UIAlertAction actionWithTitle:addString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSString  *newName = [alertController.textFields.firstObject text];
        NSString  *trimmedName = [newName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // If the user didn't input anything useful,
        // don't even bother with the completion handler.
        if (trimmedName.length > 0) {
            [self addZone:trimmedName];
        }
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:addNewObject];
    
    [self presentViewController:alertController animated:YES completion:nil];
}
//
//- (void)addNewUser {
//    [self.selectedHome addUserWithCompletionHandler:^(HMUser *user, NSError *error) {
//
//        NSLog(@"error :%@",error);
//        NSLog(@"user :%@",user);
//    }];
//}

- (void)onActionSetButtonPress:(UIButton *)sender {
    
    [self performSegueWithIdentifier:ADD_ACTIONSET_SEGUE sender:nil];
}

- (void)onTimerTriggerButtonPress:(UIButton *)sender {

    [self performSegueWithIdentifier:TIMER_TRIGGER_SEGUE sender:nil];
}

#pragma mark Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([EDIT_NAME_SEGUE isEqualToString:segue.identifier]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        EditNameTableViewController *viewController= (EditNameTableViewController *)[segue destinationViewController];
        viewController.selectedHome = selectedHome;

        switch (indexPath.section) {
            case TableViewSectionForRoom: {
                HMRoom *room = [selectedHome.rooms objectAtIndex:indexPath.row-1];
                viewController.editRoom = room;
                viewController.editNameType = EditNameTypeRoom;
            } break;
                
            case TableViewSectionForZone: {
                HMZone *zone = [selectedHome.zones objectAtIndex:indexPath.row];
                viewController.editZone = zone;
                viewController.editNameType = EditNameTypeZone;
            } break;
                
            default: break;
        }
    } else if ([@"AddedRoomsSegue" isEqualToString:segue.identifier]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        HMZone *zone = [selectedHome.zones objectAtIndex:indexPath.row];
        ZoneRoomViewController *viewController = (ZoneRoomViewController *)[segue destinationViewController];
        [viewController setSelectedZone:zone];
        [viewController setSelectedHome:selectedHome];
        
    } else if ([ADD_ACTIONSET_SEGUE isEqualToString:segue.identifier]) {
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        ActionSetViewController *viewController = (ActionSetViewController *)[navigationController topViewController];
        [viewController setSelectedHome:selectedHome];
        NSIndexPath *indexPath = (NSIndexPath *)sender;

        if (indexPath && [selectedHome.actionSets count]>indexPath.row) {
            [viewController setSelectedActionSet:selectedHome.actionSets[indexPath.row]];
        }
    } else if ([TIMER_TRIGGER_SEGUE isEqualToString:segue.identifier]) {
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        TimerTriggerViewController *viewController = (TimerTriggerViewController *)[navigationController topViewController];
        [viewController setSelectedHome:selectedHome];
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        
        if (indexPath && [selectedHome.triggers count]>indexPath.row) {
            [viewController setTrigger:selectedHome.triggers[indexPath.row]];
        }
    } else if ([@"AddAccessory" isEqualToString:segue.identifier]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        if (0 < indexPath.row) {
            HMRoom *room = [selectedHome.rooms objectAtIndex:indexPath.row-1];
            AccessoryViewController *viewController = (AccessoryViewController *)[segue destinationViewController];
            [viewController setSelectedRoom:room];
            [viewController setSelectedHome:selectedHome];
        } else {
            HMRoom *room1 = selectedHome.roomForEntireHome;
            AccessoryViewController *viewController = (AccessoryViewController *)[segue destinationViewController];
            [viewController setSelectedRoom:room1];
            [viewController setSelectedHome:selectedHome];
        }
    }
}

#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {              // Default is 1 if not implemented

    return TableViewTotalSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case TableViewSectionForRoom: return ([selectedHome.rooms count] + 1/*Added for default room*/ + 1/*Added for add new room*/) ;
        case TableViewSectionForZone: return [selectedHome.zones count] + 1/*Added for add new zone*/;
        case TableViewSectionForActionSet: return [selectedHome.actionSets count] + 1/*Added for add new Action Set*/;
        case TableViewSectionForTrigger: return [selectedHome.triggers count]  + 1/*Added for add new Timer Trigger*/;
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"RoomCell";
    RoomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.infoButton.tag=indexPath.row;
    
    
    switch (indexPath.section) {
        case TableViewSectionForRoom: {

            if (0 == indexPath.row) {
                // Default Room
                cell.roomName.text = @"Default room";
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:YES];
                [cell.roomName setHidden:NO];
                [cell.addRoomLabel setHidden:YES];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else if ([selectedHome.rooms count] > indexPath.row - 1) {
                // Change it for room
                HMRoom *room = [selectedHome.rooms objectAtIndex:indexPath.row - 1];
                [cell.roomName setHidden:NO];
                cell.roomName.text = room.name;
                [cell.infoButton setHidden:NO];
                [cell.addButton setHidden:YES];
                [cell.addRoomLabel setHidden:YES];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else {
                // Add new Room
                cell.addRoomLabel.text = @"Add Room ...";
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:NO];
                [cell.roomName setHidden:YES];
                [cell.addRoomLabel setHidden:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        } break;
            
        case TableViewSectionForZone: {
            if ([selectedHome.zones count] > indexPath.row) {
                HMZone * zone = [selectedHome.zones objectAtIndex:indexPath.row];
                cell.roomName.text = zone.name;
                [cell.infoButton setHidden:NO];
                [cell.addButton setHidden:YES];
                [cell.roomName setHidden:NO];
                [cell.addRoomLabel setHidden:YES];
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            } else {
                // Add new Zone
                cell.addRoomLabel.text = @"Add Zone ...";
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:NO];
                [cell.roomName setHidden:YES];
                [cell.addRoomLabel setHidden:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        } break;
            
        case TableViewSectionForActionSet: {
            if ([selectedHome.actionSets count] > indexPath.row) {
                HMActionSet *actionSet = selectedHome.actionSets[indexPath.row];
                cell.roomName.text = actionSet.name;
                [cell.infoButton setHidden:NO];
                [cell.addButton setHidden:YES];
                [cell.roomName setHidden:NO];
                [cell.addRoomLabel setHidden:YES];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else {
                // Add new Action Set
                cell.addRoomLabel.text = @"Add Action Set ...";
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:NO];
                [cell.roomName setHidden:YES];
                [cell.addRoomLabel setHidden:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        } break;
            
        case TableViewSectionForTrigger: {
            if ([selectedHome.triggers count] > indexPath.row) {
                HMTimerTrigger *timerTrigger = selectedHome.triggers[indexPath.row];
                cell.roomName.text = timerTrigger.name;
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:YES];
                [cell.roomName setHidden:NO];
                [cell.addRoomLabel setHidden:YES];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            } else {
                // Add new Timer Trigger
                cell.addRoomLabel.text = @"Add Timer Trigger ...";
                [cell.infoButton setHidden:YES];
                [cell.addButton setHidden:NO];
                [cell.roomName setHidden:YES];
                [cell.addRoomLabel setHidden:NO];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        } break;
            
        default: break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    // fixed font style. use custom view (UILabel) if you want something different
{
    switch (section) {
        case TableViewSectionForRoom: return @"Room Name:";
        case TableViewSectionForZone: return @"Zone Name:";
        case TableViewSectionForActionSet: return @"Action Set Name:";
        case TableViewSectionForTrigger: return @"Timer Trigger:";
        default: return nil;
    }
}

//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
//    
//    switch (section) {
////        case TableViewSectionForRoom:
//        case TableViewSectionForActionSet: return @"Tap on Action set for execution";
////        case TableViewSectionForTrigger:
//        default:     return @"";
//    }
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case TableViewSectionForRoom:
            if ((0 == indexPath.row ) || ([selectedHome.rooms count] > indexPath.row - 1)) {
                // Display Added accessory screen
                [self performSegueWithIdentifier:@"AddAccessory" sender:indexPath];
            } else {
                // Add new Room Action
                [self onAddRoomButtonPress:nil];
            }
            break;
        case TableViewSectionForZone:
            if ([selectedHome.zones count] > indexPath.row) {
                // Display added Rooms screen
                [self performSegueWithIdentifier:@"AddedRoomsSegue" sender:indexPath];
            } else {
                // Add new zone Action
                [self onAddZoneButtonPress:nil];
            }
            break;
        case TableViewSectionForActionSet: {
            if ([selectedHome.actionSets count] > indexPath.row) {
                // Execute Action Set
                HMActionSet *actionSet = [selectedHome.actionSets objectAtIndex:indexPath.row];
                [self.selectedHome executeActionSet:actionSet completionHandler:^(NSError *error) {
                    if (error) {
                        NSLog(@"execute action set error :%@",error);
                    }
                }];
            } else {
                // Add new Action Set
                [self onActionSetButtonPress:nil];
            }
        }
            break;
        case TableViewSectionForTrigger: {
            if ([selectedHome.triggers count] > indexPath.row) {
                // Display Timer Trigger
                [self performSegueWithIdentifier:TIMER_TRIGGER_SEGUE sender:indexPath];
            } else {
                // Add new Timer Trigger
                [self onTimerTriggerButtonPress:nil];
            }
        }
            break;
        default:
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case TableViewSectionForRoom:
        case TableViewSectionForZone:
            [self performSegueWithIdentifier:EDIT_NAME_SEGUE sender:indexPath];
            break;
            break;
        case TableViewSectionForActionSet:
            [self performSegueWithIdentifier:ADD_ACTIONSET_SEGUE sender:indexPath];
            break;
        case TableViewSectionForTrigger:
            break;
        default:
            break;
    }
}

//To delete Room
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case TableViewSectionForRoom: {
            // Removes a room from the home.
            if (0 == indexPath.row) {
                // Do Nothing.
            } else if ([selectedHome.rooms count] > indexPath.row - 1) {
                HMRoom *room = [self.selectedHome.rooms objectAtIndex:indexPath.row-1];
                [self.selectedHome removeRoom:room completionHandler:^(NSError *error) {
                    NSLog(@"error :%@ ",error);
                    [self.roomListTableView reloadData];
                }];
            } else {
                // Do Nothing.
            }
        } break;
        
        case TableViewSectionForZone: {
            if ([selectedHome.zones count] > indexPath.row) {
                HMZone * zone = [selectedHome.zones objectAtIndex:indexPath.row];
                [self.selectedHome removeZone:zone completionHandler:^(NSError *error) {
                    NSLog(@"error :%@ ",error);
                    [self.roomListTableView reloadData];
                }];
            } else {
                // Do Nothing.
            }

        } break;
            
        case TableViewSectionForActionSet: {
            if ([selectedHome.actionSets count] > indexPath.row) {
                // Remove Action Set
                HMActionSet *actionSet = selectedHome.actionSets[indexPath.row];
                [self.selectedHome removeActionSet:actionSet completionHandler:^(NSError *error) {
                    if (nil == error) {
                        [self.roomListTableView reloadData];
                    } else {
                        NSLog(@"Remove Action Set Error :%@ ",error);
                    }
                }];
            } else {
                // Do Nothing.
            }
        } break;

        case TableViewSectionForTrigger: {
            if ([selectedHome.triggers count] > indexPath.row) {
                // Remove Trigger
                HMTimerTrigger *timerTrigger = selectedHome.triggers[indexPath.row];
                [self.selectedHome removeTrigger:timerTrigger completionHandler:^(NSError *error) {
                    if (nil == error) {
                        [self.roomListTableView reloadData];
                    } else {
                        NSLog(@"Remove Trigger Error :%@ ",error);
                    }
                }];
            } else {
                // Do Nothing.
            }

        } break;

        default:
            break;
    }
}

// To display delete feature for rows
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
        case TableViewSectionForRoom: {
            if (0 == indexPath.row) {
                return UITableViewCellEditingStyleNone;
            } else if ([selectedHome.rooms count] > indexPath.row - 1) {
                return UITableViewCellEditingStyleDelete;
            } else {
                return UITableViewCellEditingStyleNone;
            }
        }
        case TableViewSectionForZone:
            if ([selectedHome.zones count] > indexPath.row) {
                return UITableViewCellEditingStyleDelete;
            } else {
                return UITableViewCellEditingStyleNone;
            }
        case TableViewSectionForActionSet:
            if ([selectedHome.actionSets count] > indexPath.row) {
                return UITableViewCellEditingStyleDelete;
            } else {
                return UITableViewCellEditingStyleNone;
            }
        case TableViewSectionForTrigger:
            if ([selectedHome.triggers count] > indexPath.row) {
                return UITableViewCellEditingStyleDelete;
            } else {
                return UITableViewCellEditingStyleNone;
            }
        default: return UITableViewCellEditingStyleNone;
    }
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = BACK_GROUND_HEAD_COLOR;
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor whiteColor]];
    }
}
@end
