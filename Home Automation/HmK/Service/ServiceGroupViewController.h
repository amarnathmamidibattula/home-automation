//
//  ServiceGroupViewController.h
//  HmK
//
//  Created by Santosh on 15/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;

@interface ServiceGroupViewController : UIViewController

@property (nonatomic) HMServiceGroup *serviceGroup;

@end
