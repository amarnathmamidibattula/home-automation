//
//  ServiceGroupViewController.m
//  HmK
//
//  Created by Santosh on 15/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "ServiceGroupViewController.h"

@interface ServiceGroupViewController ()

@property (nonatomic,weak) IBOutlet UITableView *serviceGroupTableView; //Displays List of Service Groups


@end

@implementation ServiceGroupViewController

@synthesize serviceGroup;
@synthesize serviceGroupTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
