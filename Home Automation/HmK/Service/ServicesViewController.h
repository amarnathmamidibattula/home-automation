//
//  ServicesViewController.h
//  HmK
//
//  Created by Santosh on 23/03/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
//  Abstract:
//  DISPLAYS THE LIST OF SERVICES OFFERED BY THE ACCESSORY

#import <UIKit/UIKit.h>
#import "CharacteristicTableViewCell.h"
@import HomeKit;

@interface ServicesViewController : UIViewController

@property (nonatomic,strong) HMAccessory *accessory;
@property (nonatomic) id<CharacteristicCellDelegate> cellDelegate;

@end
