//
//  TimerTriggerViewController.h
//  Home Automation
//
//  Created by Santosh on 20/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>
@import HomeKit;
/**
 *  TriggerViewController provides UI for creating triggers with a single kind of recurrence.
 *  It creates a table that contains action sets, a date picker, and a few recurrence interval types.
 *
 *  When the user taps the 'Save' button, all of the data associated with the view is used to
 *  initialize a new HMTimerTrigger and add it to the home.
 */

@interface TimerTriggerViewController : UITableViewController

/**
 *  The home to which this trigger will be added.
 */
@property (nonatomic, strong) HMHome *selectedHome;
/**
 *  The trigger with which to initialize this View Controller.
 */
@property (nonatomic) HMTimerTrigger *trigger;

@end
