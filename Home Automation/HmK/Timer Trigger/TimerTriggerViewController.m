//
//  TimerTriggerViewController.m
//  Home Automation
//
//  Created by Santosh on 20/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "TimerTriggerViewController.h"

/**
 * @enum This is a thing.
 */
typedef NS_ENUM(NSUInteger, TriggerTableViewSection) {
    /**
     *  The Action Sets section.
     */
    TriggerTableViewSectionName = 0,
    /**
     *  The Enabled section.
     */
    TriggerTableViewSectionEnabled,
    /**
     *  The Action Sets section.
     */
    TriggerTableViewSectionActionSet,
    /**
     *  The Action Sets section.
     */
    TriggerTableViewSectionDate,
    /**
     *  The Schedule section.
     */
    TriggerTableViewSectionRecurrence
};

static NSString *TriggerRecurrenceTitleEveryHour;
static NSString *TriggerRecurrenceTitleEveryDay;
static NSString *TriggerRecurrenceTitleEveryWeek;

@interface TimerTriggerViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UISwitch *enabledSwitch;

// Maintain a list of titles, for order, and then map them to the corresponding NSCalendarUnits.
@property (nonatomic) NSArray *recurrenceTitles;
@property (nonatomic) NSArray *recurrenceComponents;
// Keep track of which action sets the user has selected
// in order to add the right ones when we finalize the trigger.
@property (nonatomic) NSMutableArray *selectedActionSets;
// Save a dispatch_group that we can use to verify that all dispatched tasks are completed.
@property (nonatomic) dispatch_group_t saveTriggerGroup;

@property (nonatomic) NSUInteger selectedRecurrenceIndex;
/**
 *  Dismiss View Controller
 */
- (IBAction)onCancelButtonPress:(id)sender;
/**
 *  Store trigger
 */
- (IBAction)onSaveButtonPress:(id)sender;
/**
 *  Reset our saved fire date to the date in the picker.
 */
- (IBAction)didChangeDate:(UIDatePicker *)picker;

/**
 *  Any time the name field changed, reevaluate whether or not
 *  to enable the save button.
 */
- (IBAction)nameFieldDidChange:(UITextField *)sender;
/**
 *  Updates the trigger's fire date
 *
 *  @param fireDate The new trigger's fire date
 */
- (void)updateTriggerDate:(NSDate *)fireDate;
/**
 *  Creates an NSDateComponent for the selected recurrence type.
 *
 *  @return An NSDateComponent where either <code>weekOfYear</code>,
 *          <code>hour</code>, or <code>day</code> is set to 1.
 */
- (NSDateComponents *)recurrenceComponentsForSelectedIndex;
// Map the possible calendar units associated with recurrence titles, so we can properly
// set our recurrenceUnit when the user selects a cell.
- (NSUInteger)recurrenceIndexFromDateComponents:(NSDateComponents *)components;
/**
 *  Enables the save button if:
 *
 *     1. The name field is not empty, and
 *     2. There will be at least one action set in the trigger after saving.
 */
- (void)enableSaveButtonIfApplicable;
/**
 *  Creates a trigger with all of the passed-in parameters and adds it to the home, entering and
 *  leaving the dispatch group.
 *
 *  @param name                 The new trigger's name.
 *  @param fireDate             The new trigger's first fire date.
 *  @param recurrenceComponents The new trigger's recurrence components.
 */
- (void)createAndAddTriggerWithName:(NSString *)name
                           fireDate:(NSDate *)fireDate
               recurrenceComponents:(NSDateComponents *)recurrences;
/**
 *  Add Trigger to selected home, if not already added
 */
- (void)addTriggerToHomeIfNecessary:(HMTrigger *)trigger;
/**
 *  Adds the contents of self.selectedActionSets to the trigger and enables it.
 *
 *  @param trigger The trigger to which to add action sets.
 */
- (void)addActionSetsToTrigger;
/**
 *  Updates the trigger's name, entering and leaving the dispatch group if necessary.
 *  If the trigger's name is already equal to the passed-in name, this method does nothing.
 *
 *  @param name The trigger's new name.
 */
- (void)updateNameIfNecessary:(NSString *)name;
/**
 *  Updates the trigger's fire date, entering and leaving the dispatch group if necessary.
 *  If the trigger's fire date is already equal to the passed-in fire date, this method does nothing.
 *
 *  @param fireDate The trigger's new fire date.
 */
- (void)updateFireDateIfNecessary:(NSDate *)fireDate;
/**
 *  Updates the trigger's recurrence components, entering and leaving the dispatch group if necessary.
 *  If the trigger's components are already equal to the passed-in components, this method does nothing.
 *
 *  @param recurrenceComponents The trigger's new recurrence components.
 */
- (void)updateRecurrenceIfNecessary:(NSDateComponents *)recurrences;
/**
 *  Enable the trigger if necessary.
 */
- (void)enableTrigger:(HMTrigger *)trigger completionHandler:(void (^)())completion;
@end

@implementation TimerTriggerViewController

@synthesize saveButton;
@synthesize datePicker;
@synthesize nameField;
@synthesize enabledSwitch;
@synthesize recurrenceTitles;
@synthesize recurrenceComponents;
@synthesize selectedActionSets;
@synthesize saveTriggerGroup;
@synthesize selectedRecurrenceIndex;

+ (void)initialize {

    [super initialize];

    TriggerRecurrenceTitleEveryHour = NSLocalizedString(@"Every Hour", @"Every Hour");
    TriggerRecurrenceTitleEveryDay = NSLocalizedString(@"Every Day", @"Every Day");
    TriggerRecurrenceTitleEveryWeek = NSLocalizedString(@"Every Week", @"Every Week");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ActionSetCell"];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"RecurrenceCell"];
    self.navigationController.view.backgroundColor =
    [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackGround"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    // Store the possible recurrences in an array, to maintain order.
    self.recurrenceTitles = @[TriggerRecurrenceTitleEveryHour,
                              TriggerRecurrenceTitleEveryDay,
                              TriggerRecurrenceTitleEveryWeek];
    self.recurrenceComponents = @[@(NSCalendarUnitHour),
                                  @(NSCalendarUnitDay),
                                  @(NSCalendarUnitWeekOfYear)];
    // Create a dispatch group so we can properly wait for all of the individual
    // components of the saving process to finish before dismissing.
    self.saveTriggerGroup = dispatch_group_create();
    
    // If we have a trigger, set the saved properties to the current properties
    // of the passed-in trigger.
    if (self.trigger) {
        
        [self updateTriggerDate:self.trigger.fireDate];
        self.selectedActionSets = self.trigger.actionSets.mutableCopy;
        self.selectedRecurrenceIndex = [self recurrenceIndexFromDateComponents:self.trigger.recurrence];
        self.nameField.text = self.trigger.name;
        self.enabledSwitch.on = self.trigger.enabled;
        // Otherwise create new properties.
    } else {
        [self updateTriggerDate:[NSDate date]];
        self.selectedActionSets = [NSMutableArray array];
        self.selectedRecurrenceIndex = NSNotFound;
    }
    
    [self enableSaveButtonIfApplicable];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods
#pragma mark MISC Methods

- (void)enableSaveButtonIfApplicable {
    self.saveButton.enabled = self.nameField.text.length > 0 &&
    (self.selectedActionSets.count > 0 || self.trigger.actionSets.count > 0);
}

- (void)updateTriggerDate:(NSDate *)fireDate {
    unsigned flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:flags fromDate:fireDate];
    self.datePicker.date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
}

- (NSDateComponents *)recurrenceComponentsForSelectedIndex {
    if (self.selectedRecurrenceIndex == NSNotFound) {
        return nil;
    }
    NSDateComponents *recurrenceDateComponents = [NSDateComponents new];
    NSCalendarUnit unit = [self.recurrenceComponents[self.selectedRecurrenceIndex] unsignedIntegerValue];
    switch (unit) {
        case NSCalendarUnitWeekOfYear:
            recurrenceDateComponents.weekOfYear = 1;
            break;
        case NSCalendarUnitHour:
            recurrenceDateComponents.hour = 1;
            break;
        case NSCalendarUnitDay:
            recurrenceDateComponents.day = 1;
            break;
        default:
            break;
    }
    return recurrenceDateComponents;
}

- (NSUInteger)recurrenceIndexFromDateComponents:(NSDateComponents *)components {
    NSNumber *unit = nil;
    if (components.day == 1) {
        unit = @(NSCalendarUnitDay);
    } else if (components.weekOfYear == 1) {
        unit = @(NSCalendarUnitWeekOfYear);
    } else if (components.hour == 1) {
        unit = @(NSCalendarUnitHour);
    }
    return [self.recurrenceComponents indexOfObject:unit];
}

#pragma mark Save Trigger

- (void)createAndAddTriggerWithName:(NSString *)name
                           fireDate:(NSDate *)fireDate
               recurrenceComponents:(NSDateComponents *)recurrences {
    
    self.trigger = [[HMTimerTrigger alloc] initWithName:name
                                               fireDate:fireDate
                                               timeZone:nil
                                             recurrence:recurrences
                                     recurrenceCalendar:nil];
    [self addTriggerToHomeIfNecessary:self.trigger];
}

- (void)addTriggerToHomeIfNecessary:(HMTrigger *)trigger {
    if ([self.selectedHome.triggers containsObject:trigger]) {
        [self addActionSetsToTrigger];
    } else {
        dispatch_group_enter(self.saveTriggerGroup);
        __weak typeof(self) weakSelf = self;
        [self.selectedHome addTrigger:self.trigger completionHandler:^(NSError *error) {
            if (error) {
//                [weakSelf hmc_displayError:error];
            } else {
                [weakSelf addActionSetsToTrigger];
            }
            dispatch_group_leave(weakSelf.saveTriggerGroup);
        }];
    }
}

- (void)addActionSetsToTrigger {
    // Save a standard completion handler to use when
    // we either add or remove an action set.
    void (^defaultCompletion)(NSError *) = ^(NSError *error) {
        // Leave the dispatch group, to notify that we've finished this task.
        if (error) {
//            [self hmc_displayError:error];
        }
        dispatch_group_leave(self.saveTriggerGroup);
    };
    
    // First pass, remove the action sets that have been deselected.
    for (HMActionSet *actionSet in self.trigger.actionSets) {
        if ([self.selectedActionSets containsObject:actionSet]) {
            continue;
        }
        dispatch_group_enter(self.saveTriggerGroup);
        [self.trigger removeActionSet:actionSet completionHandler:defaultCompletion];
    }
    
    // Second pass, add the new action sets that were just selected.
    for (HMActionSet *actionSet in self.selectedActionSets) {
        if ([self.trigger.actionSets containsObject:actionSet]) {
            continue;
        }
        dispatch_group_enter(self.saveTriggerGroup);
        [self.trigger addActionSet:actionSet completionHandler:defaultCompletion];
    }
}

- (void)updateNameIfNecessary:(NSString *)name {
    if ([self.trigger.name isEqualToString:name]) {
        return;
    }
    dispatch_group_enter(self.saveTriggerGroup);
    [self.trigger updateName:name completionHandler:^(NSError *error) {
        if (error) {
//            [self hmc_displayError:error];
        } else {
            
        }
        dispatch_group_leave(self.saveTriggerGroup);
    }];
}

- (void)updateFireDateIfNecessary:(NSDate *)fireDate {
    if ([self.trigger.fireDate isEqualToDate:fireDate]) {
        return;
    }
    dispatch_group_enter(self.saveTriggerGroup);
    [self.trigger updateFireDate:fireDate completionHandler:^(NSError *error) {
        if (error) {
//            [self hmc_displayError:error];
        } else {
            
        }
        dispatch_group_leave(self.saveTriggerGroup);
    }];
}

- (void)updateRecurrenceIfNecessary:(NSDateComponents *)recurrences {
    if ([recurrenceComponents isEqual:self.trigger.recurrence]) {
        return;
    }
    dispatch_group_enter(self.saveTriggerGroup);
    [self.trigger updateRecurrence:recurrences completionHandler:^(NSError *error) {
        if (error) {
//            [self hmc_displayError:error];
        }
        dispatch_group_leave(self.saveTriggerGroup);
    }];
}

- (void)enableTrigger:(HMTrigger *)trigger completionHandler:(void (^)())completion {
    if (trigger.enabled == self.enabledSwitch.on) {
        completion();
        return;
    }
    [trigger enable:self.enabledSwitch.on completionHandler:^(NSError *error) {
        if (error) {
//            [self hmc_displayError:error];
//            return;
        }
        completion();
    }];
}

#pragma mark UIButton Action

- (IBAction)onCancelButtonPress:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)onSaveButtonPress:(id)sender {
    NSDateComponents *recurrences = [self recurrenceComponentsForSelectedIndex];
    NSString *name = self.nameField.text;
    NSDate *date = self.datePicker.date;
    
    if (self.trigger) {
        [self addTriggerToHomeIfNecessary:self.trigger];
        [self updateFireDateIfNecessary:date];
        [self updateRecurrenceIfNecessary:recurrences];
        [self updateNameIfNecessary:name];
    } else {
        [self createAndAddTriggerWithName:name
                                 fireDate:date
                     recurrenceComponents:recurrences];
    }

    dispatch_group_notify(self.saveTriggerGroup, dispatch_get_main_queue(), ^{
        // Update Trigger enable state.
        [self enableTrigger:self.trigger completionHandler:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    });
}

- (IBAction)didChangeDate:(UIDatePicker *)picker {
    //  Update Trigger date
    [self updateTriggerDate:picker.date];
}

- (IBAction)nameFieldDidChange:(UITextField *)sender {
    [self enableSaveButtonIfApplicable];
}

#pragma mark - Delegate Methods
#pragma mark Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case TriggerTableViewSectionActionSet:
            return self.selectedHome.actionSets.count;
        case TriggerTableViewSectionRecurrence:
            return self.recurrenceTitles.count;
        default:
            return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case TriggerTableViewSectionActionSet:
            return [self tableView:tableView actionSetCellForRowAtIndexPath:indexPath];
        case TriggerTableViewSectionRecurrence:
            return [self tableView:tableView recurrenceCellForRowAtIndexPath:indexPath];
        default:
            return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}
/**
 *  Creates a cell that represents either a selected or unselected action set cell.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView actionSetCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionSetCell" forIndexPath:indexPath];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    HMActionSet *actionSet = self.selectedHome.actionSets[indexPath.row];
    if ([self.selectedActionSets containsObject:actionSet]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = actionSet.name;
    return cell;
}

/**
 *  Creates a cell that represents a recurrence type.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView recurrenceCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecurrenceCell" forIndexPath:indexPath];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    NSString *title = self.recurrenceTitles[indexPath.row];
    // The current preferred recurrence style should have a check mark.
    if (indexPath.row == self.selectedRecurrenceIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.text = title;
    return cell;
}

#pragma mark TableView Delegate Methods
/**
 *  Tell the tableView to automatically size the custom rows, while using the superclass's
 *  static sizing for the static cells.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case TriggerTableViewSectionActionSet:
        case TriggerTableViewSectionRecurrence:
            return UITableViewAutomaticDimension;
        default:
            return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.contentView.backgroundColor = [UIColor whiteColor];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0]];
        [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor blackColor]];
        
    }
}

/**
 *  This is necessary for mixing static and dynamic table view cells.
 *  We return a fake index path because otherwise the superclass's implementation (which does not
 *  know about the extra cells we're adding) will cause an error.
 *
 *  @return The superclass's indentationLevel for the first row in the provided section,
 *          instead of the provided row.
 */
- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
    return [super tableView:tableView indentationLevelForRowAtIndexPath:newIndexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case TriggerTableViewSectionActionSet:
            [self tableView:tableView didSelectActionSetAtIndexPath:indexPath];
            break;
        case TriggerTableViewSectionRecurrence:
            [self tableView:tableView didSelectRecurrenceComponentAtIndexPath:indexPath];
            break;
        default:
            break;
    }
}

/**
 *  Handle selection of an action set cell. If the action set is already part of the selected action sets,
 *  then remove it from the selected list. Otherwise, add it to the selected list.
 */
- (void)tableView:(UITableView *)tableView didSelectActionSetAtIndexPath:(NSIndexPath *)indexPath {
    HMActionSet *actionSet = self.selectedHome.actionSets[indexPath.row];
    if ([self.selectedActionSets containsObject:actionSet]) {
        [self.selectedActionSets removeObject:actionSet];
    } else {
        [self.selectedActionSets addObject:actionSet];
    }
    [self enableSaveButtonIfApplicable];
    // Reload row
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
}

/**
 *  Handles selection of a recurrence cell. If the newly selected recurrence component is the
 *  previously selected recurrence component, reset the current selected component to <code>NSNotFound</code>
 *  and deselect that row.
 */
- (void)tableView:(UITableView *)tableView didSelectRecurrenceComponentAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *indexPaths = @[indexPath].mutableCopy;
    if (indexPath.row == self.selectedRecurrenceIndex) {
        self.selectedRecurrenceIndex = NSNotFound;
    } else {
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:self.selectedRecurrenceIndex inSection:TriggerTableViewSectionRecurrence];
        [indexPaths addObject:selectedIndexPath];
        self.selectedRecurrenceIndex = indexPath.row;
    }
    // Reload rows
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableView endUpdates];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark Text field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // called when 'return' key pressed. return NO to ignore.
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
