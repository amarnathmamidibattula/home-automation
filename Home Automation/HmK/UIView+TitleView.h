//
//  UIView+TitleView.h
//  Home Automation
//
//  Created by Amarnath on 8/26/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TitleView)

+(UIView*)getTitleView:(float)width;

@end
