//
//  UIView+TitleView.m
//  Home Automation
//
//  Created by Amarnath on 8/26/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "UIView+TitleView.h"
#define BACK_TITLE_COLOR [UIColor colorWithRed:221.0/225.0 green:84.0/225.0 blue:94.0/225.0 alpha:1.0];

@implementation UIView (TitleView)

+(UIView*)getTitleView:(float)width{
    
    UIView *navBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, width, 30)];
    navBarView.backgroundColor = BACK_TITLE_COLOR;
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(120, -10, 200, 50)];
    titleLabel.text=@"HomeKit Program";
    titleLabel.textColor=[UIColor whiteColor];
    [navBarView addSubview:titleLabel];
    return navBarView;
}

@end
