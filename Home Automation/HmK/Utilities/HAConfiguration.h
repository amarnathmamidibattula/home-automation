//
//  HAConfiguration.h
//  Home Automation
//
//  Created by Santosh on 07/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#ifndef Home_Automation_HAConfiguration_h
#define Home_Automation_HAConfiguration_h

/**
 *  Local Notification Type
 */
typedef NS_ENUM(NSInteger, LocalNotificationType) {
    BluetoothOffType = 10000,
    DisplayAccessoriesList,
};

#ifdef DEBUG
#define DebugLog(s, ...)         NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)         NSLog(s, ##__VA_ARGS__)
#endif

static const double FAR_DISTANCE_MIN_RANGE               = 4.00;
static const NSInteger FAR_RSSI_MIN_RANGE                = -89;
/*
 * Testing Event Location
 */
static const double EVENT_LOCATION_LATITUDE             = 28.620515;
static const double EVENT_LOCATION_LONGITUDE            = 77.376721;
static const double EVENT_LOCATION_RADIUS               = 100;

#endif
