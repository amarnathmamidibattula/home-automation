//
//  ZoneRoomBrowserViewController.h
//  HmK
//
//  Created by Santosh on 02/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ROOMS AVAILABLE THAT CAN BE ADDED TO THE ZONE

#import <UIKit/UIKit.h>
@import HomeKit;

@interface ZoneRoomBrowserViewController : UIViewController

@property (nonatomic, strong) HMZone *zone;
@property (nonatomic, strong) HMHome *home;

@end
