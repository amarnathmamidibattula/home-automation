//
//  ZoneRoomBrowserViewController.m
//  HmK
//
//  Created by Santosh on 02/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ROOMS AVAILABLE THAT CAN BE ADDED TO THE ZONE

#import "ZoneRoomBrowserViewController.h"
#import "HMHome+Properties.h"

@interface ZoneRoomBrowserViewController () <HMHomeDelegate, UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *zoneBrowseRoomListTable ; //Displays List of Zone
@property (nonatomic) NSMutableArray *selectedRooms;
@property (nonatomic) NSArray *displayedRooms;

@end

@implementation ZoneRoomBrowserViewController

@synthesize zoneBrowseRoomListTable;
@synthesize zone;
@synthesize home;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    
    self.selectedRooms = [NSMutableArray array];
    [self resetDisplayedRooms];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

//A METHOD FOR CANCEL BUTTON ON NAVIGATION BAR
- (void)cancel: (id)sender{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//A METHOD FOR DONE BUTTON ON NAVIGATION BAR
- (void)done: (id)sender{
    __weak typeof(self) weakSelf = self;
    [self addSelectedRoomsWithCompletionHandler:^{
         [weakSelf dismissViewControllerAnimated:YES completion:NULL];
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedRooms.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }    HMRoom *room = self.displayedRooms[indexPath.row];
    cell.textLabel.text = room.name;
    cell.accessoryType = [self.selectedRooms containsObject:room] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self handleRoomSelectionAtIndexPath:indexPath];
}

/**
 *  When an indexPath is selected, this function either adds or removes the selected room from the
 *  zone.
 */
- (void)handleRoomSelectionAtIndexPath:(NSIndexPath *)indexPath {

    // Get the room associated with this index.
    HMRoom *room = self.displayedRooms[indexPath.row];
    
    // Call the appropriate add/remove operation with the block from above.
    if ([self.selectedRooms containsObject:room]) {
        [self.selectedRooms removeObject:room];
    } else {
        [self.selectedRooms addObject:room];
    }

    [zoneBrowseRoomListTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
   
}

/**
 *  Adds the selected rooms to the zone.
 *
 *  Calls the provided completion handler once all rooms have been added.
 */
- (void)addSelectedRoomsWithCompletionHandler:(void (^)())completion {

    // Create a dispatch group for each of the room additions.
    dispatch_group_t addRoomsGroup = dispatch_group_create();
      for (HMRoom *room in self.selectedRooms) {
        dispatch_group_enter(addRoomsGroup);
        [self.zone addRoom:room completionHandler:^(NSError *error) {
               dispatch_group_leave(addRoomsGroup);
        }];
    }
    dispatch_group_notify(addRoomsGroup, dispatch_get_main_queue(), completion);
}


/**
 *  Resets the displayedRooms list to display all of the rooms in the home
 *  except the ones already inside this zone.
 */
- (void)resetDisplayedRooms {

    self.displayedRooms = [self.home hmc_roomsNotAlreadyInZone:self.zone includingRooms:self.selectedRooms];
    [self.zoneBrowseRoomListTable reloadData];
}

- (void)home:(HMHome *)home didAddRoom:(HMRoom *)room {

    [self resetDisplayedRooms];
}

- (void)home:(HMHome *)home didRemoveRoom:(HMRoom *)room {

    [self resetDisplayedRooms];
}

@end
