//
//  ZoneRoomViewController.h
//  HmK
//
//  Created by Santosh on 02/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ROOMS ADDED TO THE ZONE


#import <UIKit/UIKit.h>
@import HomeKit;

@interface ZoneRoomViewController : UIViewController

@property (nonatomic, strong) HMZone *selectedZone;
@property (nonatomic, strong) HMHome *selectedHome;

@end
