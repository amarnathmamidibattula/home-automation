//
//  ZoneRoomViewController.m
//  HmK
//
//  Created by Santosh on 02/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
// Abstract:
// DISPLAYS THE LIST OF ROOMS ADDED TO THE ZONE


#import "ZoneRoomViewController.h"
#import "ZoneRoomBrowserViewController.h"
#import "HMHome+Properties.h"

@interface ZoneRoomViewController () <HMHomeDelegate, UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *zoneAddedRoomListTable ; //Displays List of Zone


@end

@implementation ZoneRoomViewController

@synthesize zoneAddedRoomListTable;
@synthesize selectedZone;
@synthesize selectedHome;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   [self.navigationItem setTitle:selectedZone.name];
    self.zoneAddedRoomListTable.layer.cornerRadius = 10;
    self.zoneAddedRoomListTable.layer.masksToBounds = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"ZoneViewController 2");
    [super viewWillAppear:animated];
    [self.zoneAddedRoomListTable reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        ZoneRoomBrowserViewController *viewController1 = (ZoneRoomBrowserViewController *)[navigationController topViewController];
        [viewController1 setZone:selectedZone];
        [viewController1 setHome:selectedHome];
    
}

#pragma mark - Delegate and Datasource Methods
#pragma mark UITableView Data Source Methods

- (BOOL)indexPathIsAdd:(NSIndexPath *)indexPath {
    NSLog(@"ZoneViewController 4");
    return indexPath.row == self.selectedZone.rooms.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.selectedZone.rooms.count;
}

- (NSArray *)filteredRooms {
    NSLog(@"ZoneViewController 6");
    return [self.selectedHome hmc_roomsNotAlreadyInZone:self.selectedZone includingRooms:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.textColor=[UIColor darkGrayColor];
    cell.textLabel.font=[UIFont fontWithName:@"HelveticaNeue-Regular" size:16.0];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }

    HMRoom *room = self.selectedZone.rooms[indexPath.row];
    cell.textLabel.text = room.name;
    
    return cell;

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return ![self indexPathIsAdd:indexPath];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Removes a room from the home.
            HMRoom *room = [self.selectedZone.rooms objectAtIndex:indexPath.row];
            [self.selectedZone removeRoom:room completionHandler:^(NSError *error) {
            NSLog(@"error :%@ ",error);
            [self.zoneAddedRoomListTable reloadData];
        }];
    
}
@end


