//
//  CloudkitController.h
//  Home Automation
//
//  Created by Santosh on 30/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudKit/CloudKit.h>
@class RoomDetailEntity;

@interface CloudkitController : NSObject
/*
 Create the singleton object of 'CloudkitController' class.
 */
+ (instancetype)dataHandler;
/**
 *  Method used for check whether any iCloud account added in the iPhone setting
 *
 *  @param completion Returns nil ,if any, account already added.
 */
- (void)checkForiCloudAccountStatusWithCompletionHandler:(void (^)(NSError *error))completion;

- (void)storeiCloudRoomDetail:(CKRecord *)record
                   completion:(void (^)(RoomDetailEntity *roomDetailEntity))completion;
- (void)storeiCloudBeaconDetail:(NSArray *)results
                     completion:(void (^)())completion;

- (void)storeRoomDetailsOniCloud:(NSDictionary *)roomDetail
             withCompletionBlock:(void (^)(CKRecord *roomRecord, NSError *error))completion;
- (void)storeBeaconDetailsOniCloud:(NSDictionary *)beaconDetail
                   completionBlock:(void (^)(CKRecord *beaconRecord, NSError *error))completion;
- (void)storeRecord:(CKRecord *)record
  completionHandler:(void (^)(CKRecord *record, NSError *error))completionHandler;

- (void)fetchRecordsWithType:(NSString *)recordType
                   predicate:(NSPredicate *)predicate
                    recordID:(CKRecordZoneID *)recordID
             completionBlock:(void (^)(NSArray *results, NSError *error))completion;

@end
