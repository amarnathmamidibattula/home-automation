//
//  DatabaseController.m
//  Home Automation
//
//  Created by Santosh on 30/04/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "CloudkitController.h"
#import "CloudkitParams.h"
#import "HACoreDataController.h"
#import "BeaconDetailEntity.h"
#import "RoomDetailEntity.h"

@interface CloudkitController () {
    
    CKContainer *container;
    CKDatabase *publicDatabase;
    CKDatabase *privateDatabase;
}

@end

@implementation CloudkitController

static CloudkitController *dataHandler;

#pragma mark - Allocate Singleton Instance
+ (instancetype)dataHandler {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Allocate singleton instance
        dataHandler = [[CloudkitController alloc] init];
        [dataHandler initialize];
    });
    
    return dataHandler;
}

+ (id)alloc {
    @synchronized(self) {
        NSAssert(dataHandler == nil, @"Attempted to allocate a second instance of 'CloudkitController' class");
        dataHandler = [super alloc];
    }
    
    return dataHandler;
}

#pragma mark - Private Methods

- (void)initialize {

    container = [CKContainer defaultContainer];
    publicDatabase = [container publicCloudDatabase];
    privateDatabase = [container privateCloudDatabase];
}

#pragma mark - Private Methods

- (RoomDetailEntity *)createRoomDetailEntityWithId:(NSString *)roomId
                                          roomName:(NSString *)roomName
                                beaconDetailEntity:(BeaconDetailEntity *)beaconDetailEntity{

    // Check Whether Entity already exist in coredata
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"roomId == %@",roomId];
    RoomDetailEntity *entity = [[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[RoomDetailEntity description] predicate:predicate sortKey:nil] firstObject];
    
    if (nil == entity) {
        // Create RoomDetailEntity instance
        entity = (RoomDetailEntity *)[[HACoreDataController sharedInstance]
                                      createManageObjectWithName:[RoomDetailEntity description]];
        [entity setValue:roomId forKey:@"roomID"];
        [entity setValue:roomName forKey:@"roomName"];
        [entity setValue:beaconDetailEntity forKey:@"beaconDetail"];
    }

    return entity;
}

- (BeaconDetailEntity *)createBeaconEntityWithId:(NSString *)beaconId
                                            name:(NSString *)name
                                            uuid:(NSString *)uuid
                                           major:(NSUInteger)major
                                           minor:(NSUInteger)minor
                                roomDetailEntity:(RoomDetailEntity *)roomDetailEntity {
    // Check Whether Entity already exist in coredata
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"beaconID == %@",beaconId];
    BeaconDetailEntity *entity = [[[HACoreDataController sharedInstance] fetchManageObjectsWithName:[BeaconDetailEntity description] predicate:predicate sortKey:nil] firstObject];

    if (nil == entity) {
        // Create BeaconEntity instance
        entity = (BeaconDetailEntity *)[[HACoreDataController sharedInstance]
                                        createManageObjectWithName:[BeaconDetailEntity description]];
        [entity setValue:beaconId forKey:@"beaconID"];
        [entity setValue:uuid forKey:@"uuid"];
        [entity setValue:@(major) forKey:@"major"];
        [entity setValue:@(minor) forKey:@"minor"];
        [entity setValue:[NSDate date] forKey:@"index"];
        if (roomDetailEntity) {
            [entity setValue:roomDetailEntity forKey:@"roomDetail"];
        }
        
        [entity setValue:name forKey:@"beaconName"];
    }

    return entity;
}

#pragma mark Store information in Local Database
- (void)storeiCloudBeaconDetail:(NSArray *)results completion:(void (^)())completion {

    // TODO::Store Beacon detail from iCloud server to core data model
    [results enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
       
        CKRecord *record = (CKRecord *)obj;
        NSString *uuid = [record objectForKey:@"UUID"];
        NSString *beaconName = [record objectForKey:@"BeaconName"];

        NSInteger major = [[record objectForKey:@"Major"] integerValue];
        NSInteger minor = [[record objectForKey:@"Minor"] integerValue];

        CKReference *roomReference = [record objectForKey:@"RoomDetail"];
        //TODO::Check whether roomReference is nil or not?
        
        if (roomReference) {
            CKRecordID *roomRecordID = roomReference.recordID;
            
            [publicDatabase fetchRecordWithID:roomRecordID completionHandler:^(CKRecord *record, NSError *error) {
                @synchronized(self){
                    // make sure it executes on main thread
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self storeiCloudRoomDetail:record completion:^(RoomDetailEntity *roomDetailEntity) {
                            // Store Beacon Detail on Core Data
                            BeaconDetailEntity *entity = [self createBeaconEntityWithId:[NSString stringWithFormat:@"%@+%d+%d",uuid,major,minor] name:beaconName uuid:uuid major:major minor:minor roomDetailEntity:roomDetailEntity];
                        }];
                    });
                }
            }];
        } else {
            BeaconDetailEntity *entity = [self createBeaconEntityWithId:[NSString stringWithFormat:@"%@+%d+%d",uuid,major,minor] name:beaconName uuid:uuid major:major minor:minor roomDetailEntity:nil];
        }
        
        if ( [results count] == idx) {
            [[HACoreDataController sharedInstance] saveContext];
            completion ();
        }
    }];
}

- (void)storeiCloudRoomDetail:(CKRecord *)record completion:(void (^)(RoomDetailEntity *roomDetailEntity))completion {
    
    // Room ID must be Home name + Room name
    NSString *roomID = [record objectForKey:@"RoomID"];
    NSString *roomName = [record objectForKey:@"RoomName"];
    
    // Store Room Detail on Core Data
    RoomDetailEntity *roomDetailEntity = [self createRoomDetailEntityWithId:roomID roomName:roomName beaconDetailEntity:nil];
    
    [[HACoreDataController sharedInstance] saveContext];
    completion (roomDetailEntity);

    
//    CKRecordID *roomRecordId = roomDetail.recordID;
//
//    [publicDatabase fetchRecordWithID:roomRecordId completionHandler:^(CKRecord *record, NSError *error) {
//
//        if (nil == error) {
//            // Room ID must be Home name + Room name
//            NSString *roomID = [record objectForKey:@"RoomID"];
//            NSString *roomName = [record objectForKey:@"RoomName"];
//            // NSLog(@"roomID :%@",roomID);
//            // NSLog(@"roomName :%@",roomName);
//            
//            // Store Room Detail on Core Data
//            RoomDetailEntity *roomDetailEntity = [self createRoomDetailEntityWithId:roomID roomName:roomName beaconDetailEntity:nil];
//
//        } else {
//            NSLog(@"error :%@",error);
//        }
//
//    }];
}

#pragma mark - Public Methods

- (void)checkForiCloudAccountStatusWithCompletionHandler:(void (^)(NSError *error))completion {

    [container accountStatusWithCompletionHandler:^(CKAccountStatus accountStatus, NSError *error) {
        
        @synchronized(self){
            // make sure it executes on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                if (CKAccountStatusNoAccount == accountStatus) {
                    // iCloud Account not found
                    // Display Appropriate Alert Message
                    completion ([NSError errorWithDomain:@""
                                                    code:1
                                                userInfo:@{@"Title" : @"Sign in to iCloud",
                                                           @"Message" : @"Sign in to your iCloud account to write records. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn iCloud Drive on. If you don't have an iCloud account, tap Create a new Apple ID."}]);
                } else {
                    // Insert your just-in-time schema code here
                    completion (nil);
                }
            });
        }
    }];
}

#pragma mark Store Infromation On iCloud
- (void)storeRoomDetailsOniCloud:(NSDictionary *)roomDetail
             withCompletionBlock:(void (^)(CKRecord *roomRecord, NSError *error))completion {

    CKRecordID *roomRecordId = [[CKRecordID alloc] initWithRecordName:[roomDetail objectForKey:@"RoomID"]];
    // Room Record
    CKRecord *roomRecord = [[CKRecord alloc] initWithRecordType:HACloudKitRoomRecordType
                                                       recordID:roomRecordId];
    [roomRecord setObject:[roomDetail objectForKey:@"RoomID"] forKey:@"RoomID"];
    [roomRecord setObject:[roomDetail objectForKey:@"RoomName"] forKey:@"RoomName"];

    // Save the room record
    [self storeRecord:roomRecord completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!error) {
            // Insert successfully saved beacon record code
        } else {
            // Insert error handling
            NSLog(@"beaconRecord error :%@",error);
        }
        
        completion(record, error);
    }];
}

- (void)storeBeaconDetailsOniCloud:(NSDictionary *)beaconDetail
                   completionBlock:(void (^)(CKRecord *beaconRecord, NSError *error))completion {
    
    CKRecordID *beaconRecordId = [[CKRecordID alloc] initWithRecordName:[NSString stringWithFormat:@"%@+%d+%d",[beaconDetail objectForKey:@"UUID"],[[beaconDetail objectForKey:@"Major"] integerValue],[[beaconDetail objectForKey:@"Minor"] integerValue]]];
    CKRecord *beaconRecord = [[CKRecord alloc] initWithRecordType:HACloudKitBeaconRecordType
                                                         recordID:beaconRecordId];
    [beaconRecord setObject:[beaconDetail objectForKey:@"UUID"] forKey:@"UUID"];
    [beaconRecord setObject:[NSNumber numberWithInteger:[[beaconDetail objectForKey:@"Major"] integerValue]] forKey:@"Major"];
    [beaconRecord setObject:[NSNumber numberWithInteger:[[beaconDetail objectForKey:@"Minor"] integerValue]] forKey:@"Minor"];
    [beaconRecord setObject:[beaconDetail objectForKey:@"Index"] forKey:@"Index"];
    [beaconRecord setObject:[beaconDetail objectForKey:@"RoomName"] forKey:@"RoomName"];

    // Save the beacon record
    [self storeRecord:beaconRecord completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!error) {
            // Insert successfully saved beacon record code
        } else {
            // Insert error handling
            NSLog(@"beaconRecord error :%@",error);
        }
        
        completion(record, error);
    }];
}

- (void)storeRecord:(CKRecord *)record completionHandler:(void (^)(CKRecord *record, NSError *error))completionHandler {

    // Save the record
    [publicDatabase saveRecord:record completionHandler:^(CKRecord *record, NSError *error){
        
        @synchronized(self){
            // make sure it executes on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(record, error);
            });
        }
    }];
}

#pragma mark Fetch information from iCloud
/**
 *  Query cloud kit for initial fetch (thread safe)
 */
- (void)fetchRecordsWithType:(NSString *)recordType
                   predicate:(NSPredicate *)predicate
                    recordID:(CKRecordZoneID *)recordID
             completionBlock:(void (^)(NSArray *results, NSError *error))completion {
    
    CKQuery *query = [[CKQuery alloc] initWithRecordType:recordType
                                               predicate:predicate];
    [publicDatabase performQuery:query
                    inZoneWithID:recordID
               completionHandler:^(NSArray *results, NSError *error) {
                   
                   // CK convenience methods completion
                   // arent executed on the callers thread.
                   // more info: http://openradar.appspot.com/radar?id=5534800471392256
                   
                   // Update: Apple engineers replied this radar saying this is an
                   // expected behaviour and it's up to us to decide in which thread
                   // to run. I believe it's just extra work and it should always execute
                   // the completion on the thread it was originated.
                   
                   if (!error) {
                       NSLog(@"results :%@", results);
                   } else {
                       NSLog(@"FETCH ERROR: %@", error);
                   }
                   
                   @synchronized(self){
                       
                       // make sure it executes on main thread
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completion(results, error);
                       });
                   }
               }];
}

@end
