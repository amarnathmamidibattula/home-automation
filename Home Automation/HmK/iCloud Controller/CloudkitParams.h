//
//  CloudkitParams.h
//  Home Automation
//
//  Created by Santosh on 01/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#ifndef Home_Automation_CloudkitParams_h
#define Home_Automation_CloudkitParams_h


#define HACloudKitBeaconRecordType                                      @"BeaconDetail"
#define HACloudKitRoomRecordType                                        @"RoomDetail"

/**
 *  Beacon Detail Attribute list
 */
#define HACloudBeaconNameAttributeName                                  @"BeaconName"
#define HACloudUUIDAttributeName                                        @"UUID"
#define HACloudMajorAttributeName                                       @"Major"
#define HACloudMinorAttributeName                                       @"Minor"
#define HACloudIndexAttributeName                                       @"Index"

#define HACloudRoomNameAttributeName                                    @"RoomName"
#define HACloudRoomDetailAttributeName                                  @"RoomDetail"

#define HACloudRoomIDAttributeName                                      @"RoomID"


#endif
