//
//  AccessoryInformationController.m
//  Home Automation
//
//  Created by hitech on 15/07/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AccessoryInformationController.h"

@interface AccessoryInformationController ()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *infoTable;

@end

@implementation AccessoryInformationController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



