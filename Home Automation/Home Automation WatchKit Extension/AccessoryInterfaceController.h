//
//  AccessoryInterfaceController.h
//  Home Automation
//
//  Created by tcs on 26/06/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
@import HomeKit;

@interface AccessoryInterfaceController : WKInterfaceController

@end
