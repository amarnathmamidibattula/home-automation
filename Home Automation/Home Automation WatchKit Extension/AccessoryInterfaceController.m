//
//  AccessoryInterfaceController.m
//  Home Automation
//
//  Created by tcs on 26/06/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "AccessoryInterfaceController.h"
#import "RowController.h"
#import "HAHomeManager.h"

@interface AccessoryInterfaceController ()

@property (nonatomic, weak) IBOutlet WKInterfaceTable *dataTable;
@property (nonatomic, strong) HMRoom *selectedRoom;

@end

@implementation AccessoryInterfaceController

@synthesize dataTable;
@synthesize selectedRoom;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
    NSString *roomID = (NSString *)context;
    
    NSArray *components = [roomID componentsSeparatedByString:@"+"];
    NSString *homeName = [components firstObject];
    NSString *roomName = components[1];
    
    HMHomeManager *homeManager = [[HAHomeManager manager] homeManager];
    NSLog(@"home :%@",[homeManager homes]);

    NSPredicate *homePredicate = [NSPredicate predicateWithFormat:@"name == %@",homeName];
    HMHome *home = (HMHome *)[[[homeManager homes] filteredArrayUsingPredicate:homePredicate] firstObject];
    
    NSLog(@"home :%@ rooms :%@",home, home.rooms);
    
    for (HMRoom* room in home.rooms) {
        NSLog(@"room :%@", room.name);
    }
    
    NSPredicate *roomPredicate = [NSPredicate predicateWithFormat:@"name == %@",roomName];
    selectedRoom = (HMRoom *)[[home.rooms filteredArrayUsingPredicate:roomPredicate] firstObject];
    
    if (selectedRoom) {
        
    } else {
        selectedRoom = [home roomForEntireHome];
    }
//
//    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.WatchKit.sharing"];
//    self.accessories = [defaults valueForKey:@"DataSharing"];
    
    NSLog (@"accessories value is: %@", selectedRoom.accessories);
    
    [self.dataTable setNumberOfRows:[selectedRoom.accessories count] withRowType:@"rows"];
    
    for (NSInteger index=0; index < [selectedRoom.accessories count]; index++){
        
//        NSString *title = [selectedRoom.accessories objectAtIndex:index];
        HMAccessory * accessory = [selectedRoom.accessories objectAtIndex:index];
        RowController *controller = [self.dataTable rowControllerAtIndex:index];
        [controller.dataLabel setText:accessory.name];
    }
    
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex {
    
    HMAccessory * accessory = [selectedRoom.accessories objectAtIndex:rowIndex];
    [self pushControllerWithName:@"ServiceInterfaceController" context:accessory];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



