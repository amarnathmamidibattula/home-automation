//
//  HAHomeManager.h
//  Home Automation
//
//  Created by Santosh on 28/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <Foundation/Foundation.h>
@import HomeKit;

@interface HAHomeManager : NSObject

@property (nonatomic, strong) HMHomeManager * homeManager;

/*
 Create the singleton object of 'HAHomeManager' class.
 */
+ (instancetype)manager;

@end
