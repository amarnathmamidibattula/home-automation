//
//  HAHomeManager.m
//  Home Automation
//
//  Created by Santosh on 28/05/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "HAHomeManager.h"

@interface HAHomeManager ()

@end

@implementation HAHomeManager

@synthesize homeManager;

static HAHomeManager *manager;

#pragma mark - Allocate Singleton Instance
+ (instancetype)manager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Allocate singleton instance
        manager = [[HAHomeManager alloc] init];
    });
    
    return manager;
}

+ (id)alloc {
    @synchronized(self) {
        NSAssert(manager == nil, @"Attempted to allocate a second instance of 'HAHomeManager' class");
        manager = [super alloc];
    }
    
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {

        self.homeManager = [HMHomeManager new];
    }
    
    return self;
}

- (void)dealloc {
    
    self.homeManager = nil;
}

@end
