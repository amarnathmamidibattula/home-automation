//
//  InterfaceController.h
//  Home Automation WatchKit Extension
//
//  Created by tcs on 23/06/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
