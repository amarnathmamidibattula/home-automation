//
//  InterfaceController.m
//  Home Automation WatchKit Extension
//
//  Created by tcs on 23/06/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "InterfaceController.h"
#import "HAHomeManager.h"
#import "RowController.h"

@interface InterfaceController() <HMHomeManagerDelegate>
{
    HMHomeManager * homeManager;
}

@property (nonatomic, strong) HMHomeManager * homeManager;
@property (nonatomic, weak) IBOutlet WKInterfaceTable *homeInterfaceTable;

@end

@implementation InterfaceController

@synthesize homeManager;
@synthesize homeInterfaceTable;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    self.homeManager = [[HAHomeManager manager] homeManager];
    self.homeManager.delegate = self;
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (void)handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)remoteNotification {
    // when the app is launched from a notification. If launched from app icon in notification UI, identifier will be empty
    
    NSLog(@"remoteNotification :%@", [remoteNotification valueForKey:@"roomID"]);
    if([identifier isEqualToString:@"onAccessAccessoryButtonPress"]) {
        [self pushControllerWithName:@"AccessoryInterfaceController" context:[remoteNotification valueForKey:@"roomID"]];
    }
}

- (void)handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)localNotification {
// when the app is launched from a notification. If launched from app icon in notification UI, identifier will be empty
    
}

- (void)displayHomeDetail {
    
    [self.homeInterfaceTable setNumberOfRows:[self.homeManager.homes count] withRowType:@"rows"];
    
    NSLog(@"self.homeManager.homes :%@",self.homeManager.homes);
    [self.homeManager.homes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        HMHome *home = [self.homeManager.homes objectAtIndex:idx];
        RowController *controller = [self.homeInterfaceTable rowControllerAtIndex:idx];
        [controller.dataLabel setText:home.name];
    }];
}
#pragma mark HMHomeManager Delegate Methods

- (void)homeManagerDidUpdateHomes:(HMHomeManager *)manager {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self displayHomeDetail];
    });
}

- (void)homeManager:(HMHomeManager *)manager didAddHome:(HMHome *)home {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self displayHomeDetail];
    });
}

- (void)homeManager:(HMHomeManager *)manager didRemoveHome:(HMHome *)home {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self displayHomeDetail];
    });
}

@end



