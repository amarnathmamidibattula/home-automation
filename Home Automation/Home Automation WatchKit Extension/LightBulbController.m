//
//  LightBulbController.m
//  Home Automation
//
//  Created by hitech on 15/07/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "LightBulbController.h"

@interface LightBulbController () <HMAccessoryDelegate> {

    HMCharacteristic *powerChar;
    HMCharacteristic *brightnessChar;
    HMCharacteristic *saturationChar;
    HMCharacteristic *hueChar;
}

@property (nonatomic, strong) HMService *currentService;
@property (nonatomic, weak) IBOutlet WKInterfaceSwitch *powerSwitch;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *brightnessLabel;
@property (nonatomic, weak) IBOutlet WKInterfaceSlider *brightnessSlider;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *saturationLabel;
@property (nonatomic, weak) IBOutlet WKInterfaceSlider *saturationSlider;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *hueLabel;
@property (nonatomic, weak) IBOutlet WKInterfaceSlider *hueSlider;



- (IBAction)didChangePowerState:(BOOL)value;
- (IBAction)didChangeBrightness:(float)value;
- (IBAction)didChangeSaturation:(float)value;
- (IBAction)didChangeHue:(float)value;

@end

@implementation LightBulbController

@synthesize currentService;
@synthesize powerSwitch;
@synthesize brightnessSlider;
@synthesize brightnessLabel;


- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
    if ([context isKindOfClass:[HMService class]]) {
         currentService = (HMService *)context;
    }
    
    [self.currentService.characteristics enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        HMCharacteristic* charactertistic = (HMCharacteristic* )obj;
        if ([HMCharacteristicTypePowerState isEqualToString:charactertistic.characteristicType]) {
            
            powerChar = charactertistic;
            if (YES == [powerChar.value boolValue]) {
                [self.powerSwitch setOn:[powerChar.value boolValue]];
            }
            
            [self.powerSwitch setHidden:false];

//            if (!self.currentService.accessory.reachable) {
//                [self.powerSwitch setEnabled:false];
//            }            
        } else if ([HMCharacteristicTypeBrightness isEqualToString:charactertistic.characteristicType]) {
            
            brightnessChar = charactertistic;
            
            [self.brightnessSlider setValue:[brightnessChar.value floatValue]];
            
            [self.brightnessSlider setHidden:false];
            [self.brightnessLabel setHidden:false];
//            if (!self.currentService.accessory.reachable) {
//                [self.brightnessSlider setEnabled:false];
//            }
        } else if ([HMCharacteristicTypeSaturation isEqualToString:charactertistic.characteristicType]) {
            
            saturationChar = charactertistic;
            [self.saturationSlider setValue:[saturationChar.value floatValue]];
            
            [self.saturationSlider setHidden:false];
            [self.saturationLabel setHidden:false];

        } else if ([HMCharacteristicTypeHue isEqualToString:charactertistic.characteristicType]) {
            
            hueChar = charactertistic;
            
            [self.hueSlider setValue:[saturationChar.value floatValue]];
            
            [self.hueSlider setHidden:false];
            [self.hueLabel setHidden:false];

            if (!self.currentService.accessory.reachable) {
//                [self.colorsQuickGroup setHidden:false];
            }

        } else {
            NSLog(@"charactertistic.characteristicType ");
        }
    }];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    self.currentService.accessory.delegate = self;
    [self updatesCharacteristicsNotifications:true];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
    [self updatesCharacteristicsNotifications:false];
}

#pragma mark - Private Methdos

- (void)updatesCharacteristicsNotifications :(BOOL)state {
    
    if (powerChar) {
        [powerChar enableNotification:state completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Disable Notification fail, error: %@",error);
            }
        }];
    }
    
    if (brightnessChar) {
        [brightnessChar enableNotification:state completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Disable Notification fail, error: %@",error);
            }
        }];
    }
    
    if (saturationChar) {
        [saturationChar enableNotification:state completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Disable Notification fail, error: %@",error);
            }
        }];
    }

    if (hueChar) {
        [hueChar enableNotification:state completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Disable Notification fail, error: %@",error);
            }
        }];
    }
}

#pragma mark - IBAction Methods

- (IBAction)didChangePowerState:(BOOL)value {
    [powerChar writeValue:[NSNumber numberWithBool:value]
        completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Failed updating power state: %@", error);
            }
    }];
}

- (IBAction)didChangeBrightness:(float)value {
    [brightnessChar writeValue:[NSNumber numberWithFloat:value]
        completionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"Failed updating brightness: %@", error);
            }
        }];
}

- (IBAction)didChangeSaturation:(float)value {
    [saturationChar writeValue:[NSNumber numberWithFloat:value]
             completionHandler:^(NSError *error) {
                 if (error) {
                     NSLog(@"Failed updating Saturation: %@", error);
                 }
             }];
}

- (IBAction)didChangeHue:(float)value {
    [hueChar writeValue:[NSNumber numberWithFloat:value]
             completionHandler:^(NSError *error) {
                 if (error) {
                     NSLog(@"Failed updating Hue: %@", error);
                 }
             }];
}

#pragma mark - Delegate Methods
#pragma mark HMAccessoryDelegate Methods

- (void)accessory:(HMAccessory *)accessory service:(HMService *)service didUpdateValueForCharacteristic:(HMCharacteristic *)characteristic {
    
    if ([HMCharacteristicTypePowerState isEqualToString:characteristic.characteristicType]) {
        if (YES == [powerChar.value boolValue]) {
            [self.powerSwitch setOn:[powerChar.value boolValue]];
        }
    } else if ([HMCharacteristicTypeBrightness isEqualToString:characteristic.characteristicType]) {
        [self.brightnessSlider setValue:[brightnessChar.value floatValue]];
    } else if ([HMCharacteristicTypeSaturation isEqualToString:characteristic.characteristicType]) {
        [self.saturationSlider setValue:[saturationChar.value floatValue]];
    } else if ([HMCharacteristicTypeHue isEqualToString:characteristic.characteristicType]) {
        [self.hueSlider setValue:[hueChar.value floatValue]];
    } else {
        NSLog(@"Update for Char: :%@",characteristic);
    }
}

@end



