//
//  RowController.h
//  Home Automation
//
//  Created by tcs on 26/06/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//
#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface RowController : NSObject

@property (weak,nonatomic) IBOutlet WKInterfaceLabel *titleLabel;
@property (weak,nonatomic) IBOutlet WKInterfaceLabel *dataLabel ;

@end
