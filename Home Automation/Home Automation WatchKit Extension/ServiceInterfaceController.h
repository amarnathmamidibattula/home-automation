//
//  ServiceInterfaceController.h
//  Home Automation
//
//  Created by hitech on 10/07/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
@import HomeKit;

@interface ServiceInterfaceController : WKInterfaceController

@end
