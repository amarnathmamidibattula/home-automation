//
//  ServiceInterfaceController.m
//  Home Automation
//
//  Created by hitech on 10/07/15.
//  Copyright (c) 2015 Santosh. All rights reserved.
//

#import "ServiceInterfaceController.h"
#import "RowController.h"

@interface ServiceInterfaceController ()

@property (nonatomic, strong) HMAccessory* selectedAccessory;
@property (nonatomic, strong) NSDictionary *homeKitUUIDs;
@property (weak, nonatomic) IBOutlet WKInterfaceTable *serviceTable;

@end

@implementation ServiceInterfaceController

@synthesize selectedAccessory;
@synthesize homeKitUUIDs;
@synthesize serviceTable;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
    self.selectedAccessory = (HMAccessory *)context;
    [self setTitle:self.selectedAccessory.name];

    homeKitUUIDs =  @{ HMServiceTypeLightbulb:@"Light Bulb",
                       HMServiceTypeSwitch:@"Switch",
                       HMServiceTypeThermostat:@"Thermostat",
                       HMServiceTypeGarageDoorOpener:@"Garage Door Opener",
                       HMServiceTypeAccessoryInformation:@"Accessory Info",
                       HMServiceTypeFan:@"Fan",
                       HMServiceTypeOutlet:@"Outlet",
                       HMServiceTypeLockMechanism:@"Lock Mechanism",
                       HMServiceTypeLockManagement:@"Lock Management",
                       HMCharacteristicTypePowerState:@"Power State",
                       HMCharacteristicTypeHue:@"Hue",
                       HMCharacteristicTypeSaturation:@"Saturation",
                       HMCharacteristicTypeBrightness:@"Brightness",
                       HMCharacteristicTypeTemperatureUnits:@"Temperature Units",
                       HMCharacteristicTypeCurrentTemperature:@"Current Temperature",
                       HMCharacteristicTypeTargetTemperature:@"Target Temperature",
                       HMCharacteristicTypeCurrentHeatingCooling:@"Current Heating Cooling",
                       HMCharacteristicTypeTargetHeatingCooling:@"Target Heating Cooling",
                       HMCharacteristicTypeCoolingThreshold:@"Cooling Threshold",
                       HMCharacteristicTypeHeatingThreshold:@"Heating Threshold",
                       HMCharacteristicTypeCurrentRelativeHumidity:@"Current Relative Humidity",
                       HMCharacteristicTypeTargetRelativeHumidity:@"Target Relative Humidity",
                       HMCharacteristicTypeCurrentDoorState:@"Current Door State",
                       HMCharacteristicTypeTargetDoorState:@"Target Door State",
                       HMCharacteristicTypeObstructionDetected:@"Obstruction Detected",
                       HMCharacteristicTypeName:@"Name",
                       HMCharacteristicTypeManufacturer:@"Manufacturer",
                       HMCharacteristicTypeModel:@"Model",
                       HMCharacteristicTypeSerialNumber:@"Serial Number",
                       HMCharacteristicTypeIdentify:@"Identify",
                       HMCharacteristicTypeRotationDirection:@"Rotation Direction",
                       HMCharacteristicTypeRotationSpeed:@"Rotation Speed",
                       HMCharacteristicTypeOutletInUse:@"Outlet In Use",
                       HMCharacteristicTypeVersion:@"Version",
                       HMCharacteristicTypeLogs:@"Logs",
                       HMCharacteristicTypeAudioFeedback:@"Audio Feedback",
                       HMCharacteristicTypeAdminOnlyAccess:@"Admin Only Access",
                       HMCharacteristicTypeMotionDetected:@"Motion Detected",
                       HMCharacteristicTypeCurrentLockMechanismState:@"Current Lock Mechanism State",
                       HMCharacteristicTypeTargetLockMechanismState:@"Target Lock Mechanism State",
                       HMCharacteristicTypeLockMechanismLastKnownAction:@"Lock Mechanism Last Known Action",
                       HMCharacteristicTypeLockManagementControlPoint:@"Lock Management Control Point",
                       HMCharacteristicTypeLockManagementAutoSecureTimeout:@"Lock Management Auto Secure Timeout"
                    };
    
    [self displayAccessoryServices];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (void)displayAccessoryServices {
    [self.serviceTable setNumberOfRows:[self.selectedAccessory.services count] withRowType:@"rows"];
    
    [self.selectedAccessory.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

        RowController *controller = [self.serviceTable rowControllerAtIndex:idx];
        
        HMService * service = obj;
        NSString *serviceDescription = [homeKitUUIDs valueForKey:service.serviceType];
        
        if (serviceDescription) {
            [controller.dataLabel setText:serviceDescription];
        }else{
            [controller.dataLabel setText:service.serviceType];
        }
    }];
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex {
    
    NSLog(@"Did Select Row: %d",rowIndex);
    HMService * service = self.selectedAccessory.services[rowIndex];

    if ([HMServiceTypeLightbulb isEqualToString:service.serviceType]) {
        [self pushControllerWithName:@"LightBulbController" context:service];
    } else if ([HMServiceTypeThermostat isEqualToString:service.serviceType]) {
        
    } else if ([HMServiceTypeLockMechanism isEqualToString:service.serviceType]) {
        
    } else if ([HMServiceTypeGarageDoorOpener isEqualToString:service.serviceType]) {
        
    } else {
        
    }
    
//    switch (service.serviceType) {
//    case HMServiceTypeLightbulb:
//        NSLog("Light Bulb")
//        self.pushControllerWithName("LightBulbController", context: service)
//    case HMServiceTypeThermostat:
//        NSLog("Thermostat")
//        self.pushControllerWithName("ThermostatController", context: service)
//    case HMServiceTypeLockMechanism:
//        NSLog("Lock Mechanism")
//        self.pushControllerWithName("LockMechanismVC", context: service)
//    case HMServiceTypeGarageDoorOpener:
//        NSLog("Garage")
//        self.pushControllerWithName("GarageVC", context: service)
//    default:
//        NSLog("Other")
//        self.presentControllerWithName("UnsupportedServiceController", context: nil)
//    }

}

@end



